package Urb::Misc::CIESky;

use strict;
use warnings;
use 5.010;
use Math::Trig qw /pi/;

use Exporter;
use vars qw /@EXPORT_OK/;

use base qw /Exporter/;

@EXPORT_OK = qw /average_vertical average_horizontal/;

=head1 NAME

Urb::Misc::CIESky - Implements CIE average sky formulas

=head1 SYNOPSIS

Use for calculation of illumination factors depending on visible sky angle

=head1 DESCRIPTION

  use Urb::Misc::CIESky qw /average_vertical average_horizontal/

Implements some parts of CIE average sky illumination, formulas have been used
from L<http://www.bath.ac.uk/~absmaw/Lighting/C_2.pdf>

=over

=item average_vertical

   $factor = average_vertical ($radians);

Where $radians is angle of visible sky between obstruction and zenith, the
result is the illuminance factor on a vertical surface.  Note that although the
CIE sky is brighter at the zenith, a vertical surface doesn't receive much
light from above.

=item average_horizontal

Similarly:

   $factor = average_horizontal ($radians);

Where $radians is angle of visible sky between obstruction and zenith, the
result is the illuminance factor on a horizontal surface.  Note that the CIE
sky is brighter at the zenith, so obstructions at the side are not so
important.  This factor only covers the illumination from the hemisphere on the
obstruction side, so you need to do this calculation twice, once for each
hemisphere.

=back

=cut

sub average_vertical
{
    my $theta = shift;
    return (pi/6 * (1 - cos $theta)) + (4/9 * (sin $theta)**2);
}

sub average_horizontal
{
    my $theta = shift;
    return ((3 * pi * sin $theta) + (8 * $theta) + (4 * sin (2*$theta))) / 18;
}

1;
