#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Dom::Fitness') };

my $fitness = Urb::Dom::Fitness->new;

is ($fitness->Conf ('plot_ratio')->[0], 2.0, 'plot_ratio: target');
is ($fitness->Conf ('plot_ratio')->[1], 0.5, 'plot_ratio: sigma');

my $config = {plot_ratio => [1.0, 0.3],
              foo => [4, 5]};

$fitness = Urb::Dom::Fitness->new ($config);

is ($fitness->Conf ('plot_ratio')->[0], 1, 'plot_ratio: target');
is ($fitness->Conf ('plot_ratio')->[1], 0.3, 'plot_ratio: sigma');

is ($fitness->Conf ('foo')->[0], 4, 'foo: 0');
is ($fitness->Conf ('foo')->[1], 5, 'foo: 1');

ok (!$fitness->Conf ('bar'), 'bogus config item');

ok (!Urb::Dom::Fitness::debug(), 'debug()');
$Urb::Dom::Fitness::DEBUG = 1;
ok (Urb::Dom::Fitness::debug(), 'debug()');
$Urb::Dom::Fitness::DEBUG = 0;

is ($fitness->Cost ('plot'), 10, 'plot');

my $cost = {plot => 20, baz => 9};

$fitness = Urb::Dom::Fitness->new ($config, $cost);

is ($fitness->Cost ('plot'), 20, 'plot');
is ($fitness->Cost ('baz'), 9, 'baz');
ok (!$fitness->Cost ('bar'), 'bogus cost item');

1;
