#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use Data::Dumper;
use Test::More 'no_plan';
use YAML;
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use File::DXF::ENTITIES;
use Urb::Topologist;
use 5.010;

my $dxf = File::DXF->new;
$dxf->Process(read_DXF('t/data/non-manifold-4.dxf', 'CP1252'));
my @report = $dxf->{ENTITIES}->Report('POLYLINE', 'VERTEX', 'SEQEND');
my ($first_mesh) = File::DXF::ENTITIES::polyface_to_meshes(@report);
my ($nodes, $faces, $metadata) = @{$first_mesh};

my ($height, $graph, $elev);

my $topo = Urb::Topologist->new($nodes, $faces);

$topo->build_graph_nodes;
$topo->build_graph_faces;
$topo->build_graph_cells;
$topo->allocate_cells();

=cut
is (scalar $topo->cells, 2, 'cells');

is (scalar $topo->faces_vertical, 4, 'faces_vertical');
is (scalar $topo->faces_vertical_internal, 0, 'faces_vertical_internal');
is (scalar $topo->faces_vertical_external, 4, 'faces_vertical_external');
is (scalar $topo->faces_horizontal, 2, 'faces_horizontal');
is (scalar $topo->faces_horizontal_internal, 0, 'faces_horizontal_internal');
is (scalar $topo->faces_horizontal_external, 2, 'faces_horizontal_external');

is (ref $topo->walls_external, 'HASH', 'walls_external');
is (scalar keys %{$topo->walls_external}, 1, 'walls_external');
my ($elev) = keys %{$topo->walls_external};
is ($elev, 0.0, 'walls_external');
is (ref $topo->walls_external->{$elev}, 'HASH', 'walls_external');
is (scalar keys %{$topo->walls_external->{$elev}}, 1, 'walls_external');
my ($height) = keys %{$topo->walls_external->{$elev}};
like ($height, '/^2\.(7|6999)/', 'walls_external');
my $graph = $topo->walls_external->{$elev}->{$height};
is (scalar $graph->vertices, 4, 'walls_external');
is (scalar $graph->edges, 4, 'walls_external');

is (ref $topo->walls_eaves, 'HASH', 'walls_eaves');
is (scalar keys %{$topo->walls_eaves}, 1, 'walls_eaves');
($elev) = keys %{$topo->walls_eaves};
is ($elev, 0.0, 'walls_eaves');
is (ref $topo->walls_eaves->{$elev}, 'HASH', 'walls_eaves');
is (scalar keys %{$topo->walls_eaves->{$elev}}, 1, 'walls_eaves');
($height) = keys %{$topo->walls_eaves->{$elev}};
like ($height, '/^2\.(7|6999)/', 'walls_eaves');
$graph = $topo->walls_eaves->{$elev}->{$height};
is (scalar $graph->vertices, 4, 'walls_eaves');
is (scalar $graph->edges, 4, 'walls_eaves');

is (ref $topo->walls_internal, 'HASH', 'walls_internal');
is (scalar keys %{$topo->walls_internal}, 0, 'walls_internal');
($elev) = keys %{$topo->walls_internal};
is ($elev, undef, 'walls_internal');

# since this is a single cell, all cells report as external
is (keys (%{$topo->cells_internal}), 0, 'cells_internal');

=cut
#####

$dxf = File::DXF->new;
$dxf->Process(read_DXF('t/data/non-manifold-5.dxf', 'CP1252'));
@report = $dxf->{ENTITIES}->Report('POLYLINE', 'VERTEX', 'SEQEND');
($first_mesh) = File::DXF::ENTITIES::polyface_to_meshes(@report);
($nodes, $faces, $metadata) = @{$first_mesh};

$topo = Urb::Topologist->new($nodes, $faces);

$topo->build_graph_nodes;
$topo->build_graph_faces;
$topo->build_graph_cells;
$topo->allocate_cells();

is (scalar $topo->cells, 3, 'cells');

is (scalar $topo->faces_vertical, 7, 'faces_vertical');
is (scalar $topo->faces_vertical_internal, 1, 'faces_vertical_internal');
is (scalar $topo->faces_vertical_external, 6, 'faces_vertical_external');
is (scalar $topo->faces_horizontal, 4, 'faces_horizontal');
is (scalar $topo->faces_horizontal_internal, 0, 'faces_horizontal_internal');
is (scalar $topo->faces_horizontal_external, 4, 'faces_horizontal_external');

is (ref $topo->walls_external, 'HASH', 'walls_external');
is (scalar keys %{$topo->walls_external}, 1, 'walls_external');
($elev) = keys %{$topo->walls_external};
is ($elev, 0.0, 'walls_external');
is (ref $topo->walls_external->{$elev}, 'HASH', 'walls_external');
is (scalar keys %{$topo->walls_external->{$elev}}, 1, 'walls_external');
($height) = keys %{$topo->walls_external->{$elev}};
like ($height, '/^2\.(7|6999)/', 'walls_external');
$graph = $topo->walls_external->{$elev}->{$height};
is (scalar $graph->vertices, 6, 'walls_external');
is (scalar $graph->edges, 6, 'walls_external');

is (ref $topo->walls_eaves, 'HASH', 'walls_eaves');
is (scalar keys %{$topo->walls_eaves}, 1, 'walls_eaves');
($elev) = keys %{$topo->walls_eaves};
is ($elev, 0.0, 'walls_eaves');
is (ref $topo->walls_eaves->{$elev}, 'HASH', 'walls_eaves');
is (scalar keys %{$topo->walls_eaves->{$elev}}, 1, 'walls_eaves');
($height) = keys %{$topo->walls_eaves->{$elev}};
like ($height, '/^2\.(7|6999)/', 'walls_eaves');
$graph = $topo->walls_eaves->{$elev}->{$height};
is (scalar $graph->vertices, 6, 'walls_eaves');
is (scalar $graph->edges, 6, 'walls_eaves');

is (ref $topo->walls_internal, 'HASH', 'walls_internal');
is (scalar keys %{$topo->walls_internal}, 1, 'walls_internal');
($elev) = keys %{$topo->walls_internal};
is ($elev, 0.0, 'walls_internal');
is (ref $topo->walls_internal->{$elev}, 'HASH', 'walls_internal');
is (scalar keys %{$topo->walls_internal->{$elev}}, 1, 'walls_internal');
($height) = keys %{$topo->walls_internal->{$elev}};
like ($height, '/^2\.(7|6999)/', 'walls_internal');
$graph = $topo->walls_internal->{$elev}->{$height};
is (scalar $graph->vertices, 2, 'walls_internal');
is (scalar $graph->edges, 1, 'walls_internal');

is (ref $topo->cells_internal, 'HASH', 'cells_internal');
is (scalar keys %{$topo->cells_internal}, 1, 'cells_internal');
($elev) = keys %{$topo->cells_internal};
is ($elev, 0.0, 'cells_internal');
is (ref $topo->cells_internal->{$elev}, 'ARRAY', 'cells_internal');
is (scalar @{$topo->cells_internal->{$elev}}, 2, 'cells_internal');

1;
