#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use Data::Dumper;

use Test::More 'no_plan';
use Urb::Misc::Stairs qw/three_turn two_turn one_turn zero_turn risers_number ideal_going/;

# three_turn

is (three_turn (13, 0), 2, 'three_turn');
is (three_turn (13, 0.6), 2, 'three_turn');
is (three_turn (13, 1), 1, 'three_turn');
is (three_turn (13, 2), 0, 'three_turn');
is (three_turn (13, 5), 0, 'three_turn');

is (three_turn (14, 0), 2, 'three_turn');
is (three_turn (14, 0.6), 2, 'three_turn');
is (three_turn (14, 1), 1, 'three_turn');
is (three_turn (14, 2), 0, 'three_turn');
is (three_turn (14, 5), 0, 'three_turn');

is (three_turn (16, 0), 3, 'three_turn');
is (three_turn (16, 0.6), 3, 'three_turn');
is (three_turn (16, 1), 2, 'three_turn');
is (three_turn (16, 2), 1, 'three_turn');
is (three_turn (16, 3), 0, 'three_turn');
is (three_turn (16, 5), 0, 'three_turn');

is (three_turn (17, 0), 4, 'three_turn');
is (three_turn (17, 0.6), 4, 'three_turn');
is (three_turn (17, 1), 3, 'three_turn');
is (three_turn (17, 2), 2, 'three_turn');
is (three_turn (17, 3), 1, 'three_turn');
is (three_turn (17, 4), 0, 'three_turn');
is (three_turn (17, 5), 0, 'three_turn');

# two_turn

is (two_turn (13, 0), 3, 'two_turn');
is (two_turn (13, 0.6), 3, 'two_turn');
is (two_turn (13, 1), 3, 'two_turn');
is (two_turn (13, 2), 2, 'two_turn');
is (two_turn (13, 3), 2, 'two_turn');
is (two_turn (13, 6), 0, 'two_turn');
is (two_turn (13, 7), 0, 'two_turn');

is (two_turn (14, 0), 4, 'two_turn');
is (two_turn (14, 0.6), 4, 'two_turn');
is (two_turn (14, 1), 3, 'two_turn');
is (two_turn (14, 2), 3, 'two_turn');
is (two_turn (14, 3), 2, 'two_turn');
is (two_turn (14, 7), 0, 'two_turn');
is (two_turn (14, 9), 0, 'two_turn');

is (two_turn (15, 0), 4, 'two_turn');
is (two_turn (15, 0.6), 4, 'two_turn');
is (two_turn (15, 1), 4, 'two_turn');
is (two_turn (15, 2), 3, 'two_turn');
is (two_turn (15, 3), 3, 'two_turn');
is (two_turn (15, 8), 0, 'two_turn');
is (two_turn (15, 9), 0, 'two_turn');

is (two_turn (16, 0), 5, 'two_turn');
is (two_turn (16, 0.6), 5, 'two_turn');
is (two_turn (16, 1), 4, 'two_turn');
is (two_turn (16, 2), 4, 'two_turn');
is (two_turn (16, 3), 3, 'two_turn');
is (two_turn (16, 5), 2, 'two_turn');

is (two_turn (17, 0), 5, 'two_turn');
is (two_turn (17, 0.6), 5, 'two_turn');
is (two_turn (17, 1), 5, 'two_turn');
is (two_turn (17, 2), 4, 'two_turn');
is (two_turn (17, 3), 4, 'two_turn');
is (two_turn (17, 4), 3, 'two_turn');
is (two_turn (17, 10), 0, 'two_turn');
is (two_turn (17, 11), 0, 'two_turn');

# one_turn

is (one_turn (13, 0), 9, 'one_turn');
is (one_turn (13, 0.6), 9, 'one_turn');
is (one_turn (13, 1), 8, 'one_turn');
is (one_turn (13, 2), 7, 'one_turn');
is (one_turn (13, 3), 6, 'one_turn');
is (one_turn (13, 6), 3, 'one_turn');
is (one_turn (13, 9), 0, 'one_turn');
is (one_turn (13, 10), 0, 'one_turn');

is (one_turn (14, 0), 10, 'one_turn');
is (one_turn (14, 0.6), 10, 'one_turn');
is (one_turn (14, 1), 9, 'one_turn');
is (one_turn (14, 2), 8, 'one_turn');
is (one_turn (14, 3), 7, 'one_turn');
is (one_turn (14, 6), 4, 'one_turn');
is (one_turn (14, 10), 0, 'one_turn');

# zero_turn

is (zero_turn (13, 0), 12, 'zero_turn');
is (zero_turn (13, 0.6), 12, 'zero_turn');
is (zero_turn (13, 1), 12, 'zero_turn');
is (zero_turn (13, 2), 12, 'zero_turn');
is (zero_turn (13, 3), 12, 'zero_turn');
is (zero_turn (13, 12), 0, 'zero_turn');
is (zero_turn (13, 13), 0, 'zero_turn');

is (zero_turn (14, 0), 13, 'zero_turn');
is (zero_turn (14, 0.6), 13, 'zero_turn');
is (zero_turn (14, 1), 13, 'zero_turn');
is (zero_turn (14, 2), 13, 'zero_turn');
is (zero_turn (14, 3), 13, 'zero_turn');
is (zero_turn (14, 13), 0, 'zero_turn');
is (zero_turn (14, 14), 0, 'zero_turn');

is (risers_number (3.0, 0.21), 15, 'risers_number');
is (risers_number (3.0, 0.20), 15, 'risers_number');
is (risers_number (3.0, 0.19), 16, 'risers_number');

is (ideal_going (0.22), 0.22, 'ideal_going');
is (ideal_going (0.21), 0.22, 'ideal_going');
is (ideal_going (0.205), 0.22, 'ideal_going');
is (ideal_going (0.200), 0.225, 'ideal_going');
is (ideal_going (0.195), 0.235, 'ideal_going');
is (ideal_going (0.1875), 0.25, 'ideal_going');
is (ideal_going (3.0/17), 0.275, 'ideal_going');
is (ideal_going (0.150), 0.325, 'ideal_going');

1;
