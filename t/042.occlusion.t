#!/usr/bin/perl

#Editor vim:syn=perl

use 5.010;
use strict;
use warnings;
use Carp;
use Test::More 'no_plan';
use lib ('lib', '../lib');
BEGIN { use_ok('Urb::Field::Occlusion') };

my $field = Urb::Field::Occlusion->new;

local $/ = undef;
open my $YAML, '<', 't/data/field/occlusion.yml' or croak "$!";
my $yaml = (<$YAML>);
close $YAML;

ok ($field->Deserialise ($yaml), 'Deserialise()');

ok (my $result = $field->Get_interpolated_position ([0.5,0.5,0.5], 8), 'Get_interpolated_position()');

is ($result, 6.5, 'Get_interpolated_position()');
#use Data::Dumper; say Dumper $result;

#$field->{origin} = [1,2,3];

# use as an overlay
my $field2 = Urb::Field::Occlusion->new ($field);

ok ($result = $field2->Get_interpolated_position ([0.5,0.5,0.5], 8), 'Get_interpolated_position()');

is ($result, 6.5, 'Get_interpolated_position()');
