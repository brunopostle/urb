#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;
use 5.010;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };

# Tests for methods that operate on lists of Urb::Boundary objects

my $quad = Urb::Quad->new;
$quad->Deserialise (YAML::LoadFile ('t/data/quad.dom'));

#  binary tree
#         '' 
#       /     \
#     l        r
#    /  \     /  \
#  ll  lr   rl  rr
#     /   \   /   \
#    lrl lrr rrl rrr

#     quad Ids           boundary Ids
#  +---------+------+  +----c----+--c---+
#  |   rrl   |      |  d         r      |
#  +---------+  rl  |  +---rr----+      b
#  |   rrr   |      |  d         r      |
#  +------+--+------+  +--''--+''+--''--+
#  |  lrr |    lrl  |  d      lr        b
#  +------+---------+  +---l--+-----l---+
#  |        ll      |  d                b
#  +----------------+  +-------a--------+

my $boundaries = $quad->Calc_Boundaries;

for my $boundary_id (keys %{$boundaries})
{
    ok ($boundaries->{$boundary_id}->Validate_Ids, 'boundary validates');
}

is ($boundaries->{a}->Id, 'a', 'boundary a has Id = a'); 
is ($boundaries->{b}->Id, 'b', 'boundary b has Id = b'); 
is ($boundaries->{c}->Id, 'c', 'boundary c has Id = c'); 
is ($boundaries->{d}->Id, 'd', 'boundary d has Id = d'); 

is (ref ($boundaries->{a}), 'Urb::Boundary',          'boundary "a" is an Urb::Boundary object');
is (scalar @{$boundaries->{a}}, 1,                    'boundary "a" is a list with just one item');
is (scalar keys %{$boundaries->{a}->[0]}, 2,          'the item is a hash with two keys');
is (ref ($boundaries->{a}->[0]->{quad}), 'Urb::Quad', 'value of "quad" is an Urb::Quad object');
is ($boundaries->{a}->[0]->{quad}->Id, 'll',          'Id of Urb::Quad object is "ll"');
is ($boundaries->{a}->[0]->{id_edge}, '3',            'value of "id_edge" is scalar 3');

is (scalar @{$boundaries->{b}}, 3,                    'boundary "b" is a list with three items');

is (scalar @{$boundaries->{a}->Pairs}, 0, 'no paris in boundary a');
is (scalar @{$boundaries->{b}->Pairs}, 0, 'no paris in boundary b');
is (scalar @{$boundaries->{c}->Pairs}, 0, 'no paris in boundary c');
is (scalar @{$boundaries->{d}->Pairs}, 0, 'no paris in boundary d');

ok ($boundaries->{'l'}->Overlap ($quad->By_Relative_Id ('ll'), $quad->By_Relative_Id ('lrl')) > 0,
        'overlap between ll and lrl > 0.0');

ok ($boundaries->{'l'}->Overlap ($quad->By_Relative_Id ('ll'), $quad->By_Relative_Id ('lrr')) > 0,
        'overlap between ll and lrr > 0.0');

like ($boundaries->{'l'}->Bearing ($quad->L->L, $quad->L->R->L), '/^1\.99/', 'Bearing');
like ($boundaries->{'l'}->Bearing ($quad->L->L, $quad->L->R->R), '/^1\.99/', 'Bearing');
like ($boundaries->{'l'}->Bearing ($quad->L->R->L, $quad->L->L), '/^5\.13/', 'Bearing');
like ($boundaries->{'l'}->Bearing ($quad->L->R->R, $quad->L->L), '/^5\.13/', 'Bearing');

like ($boundaries->{'lr'}->Bearing ($quad->L->R->L, $quad->L->R->R), '/^3\.06/', 'Bearing');
like ($boundaries->{'lr'}->Bearing ($quad->L->R->R, $quad->L->R->L), '/^6\.20/', 'Bearing');

is ($boundaries->{'l'}->Pairs->[0]->[0]->Id, 'll', 'first pair is ll and lrl');
is ($boundaries->{'l'}->Pairs->[0]->[1]->Id, 'lrl', 'first pair is ll and lrl');
is ($boundaries->{'l'}->Pairs->[1]->[0]->Id, 'll', 'second pair is ll and lrr');
is ($boundaries->{'l'}->Pairs->[1]->[1]->Id, 'lrr', 'second pair is ll and lrr');
is (defined $boundaries->{'l'}->Pairs->[2], '', 'there is no third pair');

my $boundary = $boundaries->{'l'};

is (scalar @{$boundary->Pairs}, 2, 'there are two links in this boundary');
is (scalar @{$boundary->Pairs_By_Length}, 2, 'there are two links in this boundary');

for my $boundary_id (keys %{$boundaries})
{
    # FIXME abcd boundaries should be queryable too
    next if $boundary_id =~ /^[abcd]$/x;
    $boundary = $boundaries->{$boundary_id};
    ok ($boundary->Overlap (@{$boundary->Pairs_By_Length->[0]}) <=
        $boundary->Overlap (@{$boundary->Pairs_By_Length->[-1]}),'shortest overlap is shortest');
}

is (scalar @{$boundaries->{''}}, 4, '4 quads adjacent to \'\'');
is (scalar @{$boundaries->{l}}, 3, '3 quads adjacent to l');
is (scalar @{$boundaries->{r}}, 3, '3 quads adjacent to r');
is (scalar @{$boundaries->{lr}}, 2, '2 quads adjacent to lr');
is (scalar @{$boundaries->{rr}}, 2, '2 quads adjacent to rr');
is (scalar @{$boundaries->{a}}, 1, '1 quads adjacent to a');
is (scalar @{$boundaries->{b}}, 3, '3 quads adjacent to b');
is (scalar @{$boundaries->{c}}, 2, '2 quads adjacent to c');
is (scalar @{$boundaries->{d}}, 4, '4 quads adjacent to d');

ok ($boundaries->{''}->Coordinates ($quad->By_Id ('rrr'), $quad->By_Id ('lrr')), 'rrr/lrr');
ok ($boundaries->{''}->Coordinates ($quad->By_Id ('rl'), $quad->By_Id ('lrl')), 'rl/lrl');
ok ($boundaries->{''}->Coordinates ($quad->By_Id ('lrl'), $quad->By_Id ('rrr')), 'lrl/rrr');
ok (!$boundaries->{''}->Coordinates ($quad->By_Id ('rl'), $quad->By_Id ('lrr')), 'rl/lrr');
ok (!$boundaries->{''}->Coordinates ($quad->By_Id ('rl'), $quad->By_Id ('rrr')), 'rl/rrr');
ok (!$boundaries->{''}->Coordinates ($quad->By_Id ('ll'), $quad->By_Id ('lrr')), 'll/lrr');
ok (!$boundaries->{''}->Coordinates ($quad->By_Id ('lrr'), $quad->By_Id ('ll')), 'lrr/ll');

ok ($boundaries->{''}->Overlap ($quad->By_Id ('rrr'), $quad->By_Id ('lrr')), 'rrr/lrr');
ok ($boundaries->{''}->Overlap ($quad->By_Id ('rl'), $quad->By_Id ('lrl')), 'rl/lrl');
ok ($boundaries->{''}->Overlap ($quad->By_Id ('lrl'), $quad->By_Id ('rrr')), 'lrl/rrr');
ok ($boundaries->{''}->Overlap ($quad->By_Id ('rl'), $quad->By_Id ('lrr')) <= 0, 'rl/lrr');
ok ($boundaries->{''}->Overlap ($quad->By_Id ('rl'), $quad->By_Id ('rrr')) <= 0, 'rl/rrr');
ok ($boundaries->{''}->Overlap ($quad->By_Id ('ll'), $quad->By_Id ('lrr')) <= 0, 'll/lrr');
ok ($boundaries->{''}->Overlap ($quad->By_Id ('lrr'), $quad->By_Id ('ll')) <= 0, 'lrr/ll');

# lets break a boundary

ok ($boundaries->{''}->Validate_Ids, 'boundary validates');
$boundaries->{''}->[0]->{id_edge} = '3';
ok (!$boundaries->{''}->Validate_Ids, 'boundary doesnt validate');
$boundaries->{''}->[0]->{id_edge} = '0';
ok ($boundaries->{''}->Validate_Ids, 'boundary validates');

my $q = Urb::Quad->new;
$q->{node} = [[1.0,0.0],[6.0,1.0],[5.0,6.0],[0.0,5.0]];
$q->Divide (0.75, 0.75);
my $bs = $q->Calc_Boundaries;
like ($bs->{''}->Bearing ($q->L, $q->R), '/^0.197/', 'Bearing');
like ($bs->{''}->Bearing ($q->R, $q->L), '/^3.338/', 'Bearing');

$q->Rotate; $q->Rotate;
$bs = $q->Calc_Boundaries;
like ($bs->{''}->Bearing ($q->R, $q->L), '/^0.197/', 'Bearing');
like ($bs->{''}->Bearing ($q->L, $q->R), '/^3.338/', 'Bearing');

$q->Rotate;
$bs = $q->Calc_Boundaries;
like ($bs->{''}->Bearing ($q->R, $q->L), '/^1.76/', 'Bearing');
like ($bs->{''}->Bearing ($q->L, $q->R), '/^4.90/', 'Bearing');

