#!/usr/bin/perl

#Editor vim:syn=perl

use 5.010;
use strict;
use warnings;
use Carp;
use Test::More 'no_plan';
use lib ('lib', '../lib');
BEGIN { use_ok('Urb::Field') };

my $field = Urb::Field->new;

local $/ = undef;
open my $YAML, '<', 't/data/field/eight.yml' or croak "$!";
my $yaml = (<$YAML>);
close $YAML;

$field->Deserialise ($yaml);

is ($field->Get_int ([0,0,0]), 2, 'Get_int()');
is ($field->Get_int ([1,0,0]), 4, 'Get_int()');
is ($field->Get_int ([0,1,0]), 9, 'Get_int()');
is ($field->Get_int ([1,1,0]), 6, 'Get_int()');
is ($field->Get_int ([0,0,1]), 3, 'Get_int()');
is ($field->Get_int ([0,1,1]), 5, 'Get_int()');
is ($field->Get_int ([1,0,1]), 1, 'Get_int()');
is ($field->Get_int ([1,1,1]), 7, 'Get_int()');

my $yaml2 = $field->Serialise;

is ($yaml, $yaml2, 'Serialised Deserialised identical');

ok ($field->Get_interpolated ([0.0,0,0]) < $field->Get_interpolated ([0.1,0,0]), 'Get_interpolated');
ok ($field->Get_interpolated ([0.2,0,0]) < $field->Get_interpolated ([0.3,0,0]), 'Get_interpolated');
ok ($field->Get_interpolated ([0.4,0,0]) < $field->Get_interpolated ([0.5,0,0]), 'Get_interpolated');
ok ($field->Get_interpolated ([0.6,0,0]) < $field->Get_interpolated ([0.7,0,0]), 'Get_interpolated');
ok ($field->Get_interpolated ([0.8,0,0]) < $field->Get_interpolated ([0.9,0,0]), 'Get_interpolated');

is ($field->Get_interpolated ([1.0,0,0]), $field->Get_interpolated ([1.5,0,0]), 'Get_interpolated');
is ($field->Get_interpolated ([0.0,0,0]), $field->Get_interpolated ([-0.5,0,0]), 'Get_interpolated');

is ($field->Get_interpolated ([0.5,0.5,0.5]), 4.625, 'Get_interpolated');

1;
