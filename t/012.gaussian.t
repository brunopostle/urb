#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use Data::Dumper;

use Test::More 'no_plan';
use Urb::Math qw /gaussian/;

# $x = input value
# $a = scale (usually 1.0)
# $b = centre of peak
# $c = sigma

is (gaussian (1.57, 1.0, 1.57, 0.1), 1, 'gaussian');
ok (gaussian (0, 1.0, 1.57, 0.1) < 0.00001, 'gaussian');
ok (gaussian (0.1, 1.0, 1.570796, 0.3) < 0.01, 'v actute corner low score');
ok (gaussian (3.1, 1.0, 1.570796, 0.3) < 0.01, 'v obtuse corner low score');
ok (gaussian (1.57, 1.0, 1.570796, 0.3) > 0.99, 'right angle corner high score');
ok (gaussian (1.52, 1.0, 1.570796, 0.3) > 0.98, 'good corner');
ok (gaussian (1.62, 1.0, 1.570796, 0.3) > 0.98, 'good corner');
ok (gaussian (1.47, 1.0, 1.570796, 0.3) < 0.95, 'bad corner');
ok (gaussian (1.67, 1.0, 1.570796, 0.3) < 0.95, 'bad corner');

is (gaussian (1.5, 1.0, 1.5, 0.5), 1, '3:2 is just right');
like (gaussian (1.6, 1.0, 1.5, 0.5), '/^0.98/', 'gaussian');
like (gaussian (1.8, 1.0, 1.5, 0.5), '/^0.83/', 'gaussian');
like (gaussian (1.9, 1.0, 1.5, 0.5), '/^0.72/', 'gaussian');
like (gaussian (2.0, 1.0, 1.5, 0.5), '/^0.60/', '2:1 not so good');
like (gaussian (2.5, 1.0, 1.5, 0.5), '/^0.13/', 'gaussian');
like (gaussian (3.0, 1.0, 1.5, 0.5), '/^0.01/', '3:1 just won\'t do');


