#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Quad;

my $quad = Urb::Quad->new;

$quad->{node} = [[0.0,0.0],[16.0,1.0],[15.0,16.0],[-1.0,15.0]];

$quad->Divide (0.75, 0.75);
$quad->L->Divide (0.6, 0.6);
$quad->L->L->Divide (0.5, 0.5);
$quad->R->Divide (0.7, 0.7);
$quad->R->Rotate;

is (scalar $quad->Levels_Above, 0, 'one level');
ok ($quad->Add_Above, 'Add_Above()');
is (scalar $quad->Levels_Above, 1, 'two levels');

ok (!$quad->Add_Above, 'Add_Above() fails unless top level');
is (scalar $quad->Levels_Above, 1, 'two levels');

is ($quad, $quad->Above->Below, 'Above(), Below()');
is ($quad, $quad->Above->Lowest, 'Lowest()');
is ($quad, $quad->Above->By_Relative_Level (-1), 'By_Relative_Level()');
is ($quad, $quad->Above->By_Level (0), 'By_Level()');
is ($quad, $quad->By_Level (0), 'By_Level()');
is ($quad->Above, $quad->By_Relative_Level (1), 'By_Relative_Level()');

is (scalar ($quad->Levels_Above), 1, 'Levels_Above()');
is (scalar ($quad->Above->Levels_Below), 1, 'Levels_Below()');

$quad->Above->Divide (0.41, 0.47);
$quad->Above->R->Divide (0.3, 0.3);

is_deeply ($quad->Coordinate_a, $quad->Above->Coordinate_a, 'Coordinate_a()');
is_deeply ($quad->Coordinate_b, $quad->Above->Coordinate_b, 'Coordinate_b()');

$quad->Above->Rotate;

is ($quad->Rotation, $quad->Above->Rotation, 'Rotation()');

is (scalar $quad->Levels_Above, 1, 'two levels');
ok ($quad->Above->Add_Above, 'Add_Above()');
is (scalar $quad->Levels_Above, 2, 'three levels');
$quad->Above->Above->Divide (0.35, 0.36);
$quad->Above->Above->L->Divide (0.22, 0.24);

is ($quad->Above->Above->Below->Below, $quad, 'Above->Above()'); 

is (scalar ($quad->Levels_Above), 2, 'Levels_Above()');
is (scalar ($quad->Above->Above->Levels_Below), 2, 'Levels_Below()');

is ($quad->Level, 0, 'Level()');
is ($quad->Above->Level, 1, 'Level()');
is ($quad->Above->Above->Level, 2, 'Level()');

is_deeply ($quad->Coordinate_a, $quad->Above->Above->Coordinate_a, 'Coordinate_a()');
is_deeply ($quad->Coordinate_b, $quad->Above->Above->Coordinate_b, 'Coordinate_b()');

is_deeply ($quad->R->Coordinate_a, $quad->Above->R->Coordinate_a, 'Coordinate_a()');
is_deeply ($quad->R->Coordinate_b, $quad->Above->R->Coordinate_b, 'Coordinate_b()');

is_deeply ($quad->L->Coordinate_a, ['7.2','0.45'], 'Coordinate_a()');

is ($quad->Above->L->Coordinate_a, undef, 'Coordinate_a()');
is_deeply ($quad->Above->Above->L->Coordinate_a, ['2.64','0.165'], 'Coordinate_a()');

is ($quad->By_Level (0), $quad, 'By_Level()');
is ($quad->By_Level (1), $quad->Above, 'By_Level()');
is ($quad->By_Level (2), $quad->Above->Above, 'By_Level()');
is ($quad->Above->By_Level (0), $quad, 'By_Level()');
is ($quad->Above->By_Level (1), $quad->Above, 'By_Level()');
is ($quad->Above->By_Level (2), $quad->Above->Above, 'By_Level()');

is ($quad->L->L->L->Above_More->Id, 'l', 'Above_More()');
is ($quad->R->L->Above_More->Id, 'rl', 'Above_More()');
is ($quad->Above->Above->L->L->Below_More->Id, 'l', 'Below_More()');

my @foo = $quad->Above_Leafs;
is (scalar (@foo), 3, 'Above_Leafs()');
ok ($quad->Vertical_Connection ($foo[0]) > 0.0, 'Vertical_Connection()');
ok ($quad->Vertical_Connection ($foo[1]) > 0.0, 'Vertical_Connection()');
ok ($quad->Vertical_Connection ($foo[2]) > 0.0, 'Vertical_Connection()');

@foo = $quad->L->L->L->Above_Leafs;
is (scalar (@foo), 1, 'Above_Leafs()');
is ($foo[0]->Id, 'l', 'Above_Leafs()');
ok ($quad->Vertical_Connection ($foo[0]) > 0.0, 'Vertical_Connection()');

@foo = $quad->Above->Above->L->L->Below_Leafs;
is (scalar (@foo), 1, 'Below_Leafs()');
is ($foo[0]->Id, 'l', 'Below_Leafs(');

my $yaml = YAML::Dump ($quad->Serialise);
my @list = $quad->Serialise;
my $quad_b = Urb::Quad->new;
$quad_b->Deserialise (@list);
$quad->Clean_Cache;
$quad_b->Clean_Cache;
is_deeply ($quad, $quad_b, 'deserialised serialised quad is identical') if $Test::More::VERSION >= 0.49;

$quad->Del_Above;
is ($quad->Levels_Above, 0, 'Del_Level()');

1;
