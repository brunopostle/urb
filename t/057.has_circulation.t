#!/usr/bin/perl

#Editor vim:syn=perl

use 5.010;
use strict;
use warnings;
use Test::More 'no_plan';
use lib ('lib', '../lib');
use_ok ('Urb::Dom');
use Graph;

my $dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/simple3.dom')), 'deserialise');

my $graph = $dom->Graph;

is (scalar $graph->vertices, 3, '3 vertices');
is (scalar $graph->edges, 3, '3 edges');

ok ($dom->Has_Circulation ($graph), 'Has_Circulation()');

is (scalar $graph->vertices, 3, '3 vertices');
is (scalar $graph->edges, 2, '2 edges');

my $l = $dom->By_Id('l')->Divide (0.75,0.75);

$graph = $dom->Graph;
is (scalar $graph->vertices, 4, '4 vertices');
is (scalar $graph->edges, 5, '5 edges');
ok ($dom->Has_Circulation ($graph), 'Has_Circulation()');
is (scalar $graph->vertices, 4, '4 vertices');
is (scalar $graph->edges, 3, '3 edges');

is ($dom->By_Id('rr')->Type, 'B', 'rr is bedroom');

$dom->By_Id('rr')->Type ('L');
is ($dom->By_Id('rr')->Type, 'L', 'rr is living');

$graph = $dom->Graph;
is (scalar $graph->vertices, 4, '4 vertices');
is (scalar $graph->edges, 5, '5 edges');
ok ($dom->Has_Circulation ($graph), 'Has_Circulation()');
is (scalar $graph->vertices, 4, '4 vertices');
is (scalar $graph->edges, 3, '3 edges');

$dom->By_Id('ll')->Type ('S');
is ($dom->By_Id('ll')->Type, 'S', 'll is sahn');

$graph = $dom->Graph;
is (scalar $graph->vertices, 4, '4 vertices');
is (scalar $graph->edges, 5, '5 edges');
ok ($dom->Has_Circulation ($graph), 'Has_Circulation()');
is (scalar $graph->vertices, 4, '4 vertices');
is (scalar $graph->edges, 3, '3 edges');

$dom->By_Id('rl')->Type ('K');
is ($dom->By_Id('rl')->Type, 'K', 'll is kitchen');

$graph = $dom->Graph;
is (scalar $graph->vertices, 4, '4 vertices');
is (scalar $graph->edges, 5, '5 edges');
ok ($dom->Has_Circulation ($graph), 'Has_Circulation()');
is (scalar $graph->vertices, 4, '4 vertices');
is (scalar $graph->edges, 4, '4 edges');

1;
