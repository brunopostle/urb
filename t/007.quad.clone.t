#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };

# Tests for methods that manipulate the tree with Clone() and Crossover()

my $quad = Urb::Quad->new;
$quad->Deserialise (YAML::LoadFile ('t/data/quad.dom'));

#  binary tree
#         '' 
#       /     \
#     l        r
#    /  \     /  \
#  ll  lr   rl  rr
#     /   \   /   \
#    lrl lrr rrl rrr

#     quad Ids           boundary Ids
#  +---------+------+  +----c----+--c---+
#  |   rrl   |      |  d         r      |
#  +---------+  rl  |  +---rr----+      b
#  |   rrr   |      |  d         r      |
#  +------+--+------+  +--''--+''+--''--+
#  |  lrr |    lrl  |  d      lr        b
#  +------+---------+  +---l--+-----l---+
#  |        ll      |  d                b
#  +----------------+  +-------a--------+

my $clone = $quad->By_Id ('rrr')->Clone;

is ($clone->Position, '', 'position of root is empty string');
is ($clone->Id, '', 'Root level Id is empty string');
is ($clone->Coordinate (1)->[0], $quad->By_Id ('rrr')->Coordinate (1)->[0], 'Coordinate');
is ($clone->Coordinate (1)->[1], $quad->By_Id ('rrr')->Coordinate (1)->[1], 'Coordinate');
is ($clone->Divided, 0, 'Not divided');

my $a = $quad->Clone;
ok ($a->By_Id ('rr')->Divided && ! $a->By_Id ('rl')->Divided, 'rr is divided not rl');
$a->By_Id ('rl')->Crossover ($a->By_Id ('rr'));
ok (! $a->By_Id ('rr')->Divided && $a->By_Id ('rl')->Divided, 'rl is divided not rr');

ok ($a->By_Id ('rl')->Divided, 'crossover');
ok (! $a->By_Id ('rr')->Divided, 'crossover');

my $b = $quad->Clone;
my $c = $quad->Clone;
$b->Crossover ($c->Right->Right->Right);

my @leafs = $b->Leafs;
is (scalar @leafs, 1, 'one leafs');

@leafs = $c->Leafs;
is (scalar @leafs, 11, 'eleven leafs');

ok (! $c->Crossover ($c->Right->Right), 'don\'t crossover when descendant');
ok (! $c->Right->Right->Crossover ($c), 'don\'t crossover when descendant');

my $quad2 = Urb::Quad->new;
$quad2->{node} = [[1.0,1.0],[4.0,2.0],[4.0,6.0],[2.0,4.0]];
$quad2->Divide (0.5, 0.5);
$quad2->Rotate;
is ($quad2->Left->Undivide, 0, 'undivide leaf returns 0');
is ($quad2->Undivide, 1, 'undivide branch returns 1');
is ($quad2->Undivide, 0, 'undivide single quad returns 0');

is ($quad2->Swap, 0, 'swap childless quad returns 0');

1;
