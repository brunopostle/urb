#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };

my $quad = Urb::Quad->new;

$quad->{node} = [[4,2],[13,3],[13,14],[5,11]];
$quad->Divide (0.5, 0.5);

is_deeply ($quad->Coordinate_a, [8.5,2.5] , 'coor_a');
is_deeply ($quad->Coordinate_b, [9,12.5] , 'coor_b');

ok ($quad->Straighten == 0, 'straighten fails');

$quad->L->Divide (0.5, 0.5);
$quad->L->Rotate;

$quad->L->Straighten;

like ($quad->L->Coordinate_b->[0], '/^4.633/', 'Coordinate()');

is_deeply ($quad->Coordinate_a, [8.5,2.5], 'Coordinate_a()');
is_deeply ($quad->Coordinate_b, [9.0,12.5], 'Coordinate_b()');

ok ($quad->Straighten_Root, 'straighten_root succeeds');

is_deeply ($quad->Coordinate_a, [8.5,2.5], 'Coordinate_a()');
is_deeply ($quad->Coordinate_b, [2236/300,11.92], 'Coordinate_b()');

$quad->L->Straighten;

like ($quad->L->Coordinate_b->[0], '/^4.536/', 'Coordinate_b()');

$quad->Rotate;
$quad->Divide (0.5, 0.5);

$quad->L->Straighten;
ok ($quad->Straighten_Root, 'straighten_root succeeds');
$quad->L->Straighten;

is ($quad->Coordinate_a->[1], 8.5, 'Coordinate_a()');
is ($quad->Coordinate_b->[1], 8.5, 'Coordinate_b()');

ok (abs ($quad->L->Coordinate_a->[0]) - abs ($quad->L->Coordinate_b->[0]) < 0.00001, 'Coordinate_a and Coordinate_b');

