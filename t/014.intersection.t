#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use Data::Dumper;
use Urb::Math qw /points_2line line_intersection/;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };

my $line = points_2line ([6,4], [9,5]);
is ($line->{a}, 1/3, 'gradient');
is ($line->{b}, 2, 'offset');

$line = points_2line ([-5,7], [-1,3]);
is ($line->{a}, -1, 'gradient');
is ($line->{b}, 2, 'offset');

$line = points_2line ([-1,3], [-5,7]);
is ($line->{a}, -1, 'gradient');
is ($line->{b}, 2, 'offset');

$line = points_2line ([-5,7], [4,-2]);
is ($line->{a}, -1, 'gradient');
is ($line->{b}, 2, 'offset');

my $line1 = points_2line ([6,4], [9,5]);
my $line2 = points_2line ([-5,7], [-1,3]);

my $intersection = line_intersection ($line1, $line2);
is ($intersection->[0], 0, 'intersection');
is ($intersection->[1], 2, 'intersection');

$intersection = line_intersection ($line2, $line1);
is ($intersection->[0], 0, 'intersection');
is ($intersection->[1], 2, 'intersection');
