#!/usr/bin/perl

#Editor vim:syn=perl

use 5.010;
use strict;
use warnings;
use Test::More 'no_plan';
use lib ('lib', '../lib');
use_ok ('Urb::Dom');
use Graph;

my $dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/multilevel.dom')), 'deserialise');

my $graph = $dom->Graph;
my $clone = $graph->clone;

is (scalar $graph->vertices, 8, '8 vertices');
is (scalar $clone->vertices, 8, '8 vertices');

ok ($dom->Has_Circulation ($graph), 'Has_Circulation()');

is (scalar $graph->vertices, 8, '8 vertices');
is (scalar $clone->vertices, 8, '8 vertices');

for my $vertex ($graph->vertices)
{
    ok ($graph->average_path_length ($vertex, undef) > 0, 'average_path_length');
}

is (scalar $graph->sorted_apl, 8, '8 vertices sorted_apl()');
is (scalar $clone->sorted_apl, 8, '8 vertices sorted_apl()');

my $foo = $graph->clone;
my $bar = $graph->clone;

ok ($dom->Connected_Circulation ($foo), 'yes Connected_Circulation()');
ok (!$dom->Connected_Outside ($bar), 'no Connected_Outside()');

is (scalar $graph->vertices, 8, '8 vertices');
is (scalar $foo->vertices, 1, '1 vertices');
is (scalar $bar->vertices, 3, '3 vertices');

is (scalar $graph->connected_components, 1, '1 connected component');
is (scalar $foo->connected_components, 1, '1 connected component');
is (scalar $bar->connected_components, 3, '3 connected components');

###

my $c1 = Urb::Dom->new;
$c1->Type ('c');

my $c2 = Urb::Dom->new;
$c2->Type ('c');

my $c3 = Urb::Dom->new;
$c3->Type ('c');

my $b1 = Urb::Dom->new;
$b1->Type ('b');

my $b2 = Urb::Dom->new;
$b2->Type ('b');

my $l1 = Urb::Dom->new;
$l1->Type ('l');

my $t1 = Urb::Dom->new;
$t1->Type ('t');

my $k1 = Urb::Dom->new;
$k1->Type ('k');

my $g = Graph::->new (undirected => 1, refvertexed => 1);
$g->add_weighted_edge ($b1, $b2, 4);
$g->add_weighted_edge ($b1, $c2, 2);
$g->add_weighted_edge ($b1, $c1, 3);
$g->add_weighted_edge ($b2, $c1, 3);
$g->add_weighted_edge ($b2, $k1, 5);
$g->add_weighted_edge ($c1, $k1, 3);
$g->add_weighted_edge ($l1, $k1, 2);
$g->add_weighted_edge ($l1, $c1, 3);
$g->add_weighted_edge ($c1, $c2, 5);
$g->add_weighted_edge ($l1, $c2, 4);
$g->add_weighted_edge ($l1, $t1, 2);
$g->add_weighted_edge ($t1, $c2, 2);
$g->add_weighted_edge ($t1, $c3, 1);
$g->add_weighted_edge ($c2, $c3, 3);
is (scalar $g->edges, 14, '14 edges');

my $g2 = $g->clone;

ok ($b1->Has_Circulation ($g), 'Has_Circulation()');
is (scalar $g->edges, 8, '8 edges');

ok ($b1->Has_Circulation ($g), 'Has_Circulation()');
is (scalar $g->edges, 8, '8 edges');

