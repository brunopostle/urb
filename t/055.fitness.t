#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;
use Urb::Dom::Fitness;
use YAML;

my $dom = Urb::Dom->new;
my $fitness = Urb::Dom::Fitness->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/wall-length.dom')), 'deserialise');

$fitness->{_occlusion} = $dom->Occlusion ();
$fitness->{_occlusion}->{nocache} = 1;

my $score_1 = sprintf ("%.20f", $fitness->_apply ($dom));
my $score_2 = sprintf ("%.20f", $fitness->_apply ($dom));
my $score_3 = sprintf ("%.20f", $fitness->_apply ($dom));

is ($score_1, $score_2, 'consecutive fitnesses');
is ($score_2, $score_3, 'consecutive fitnesses');
is ($score_1, $score_3, 'consecutive fitnesses');

#print join "\n", $dom->Failures;
