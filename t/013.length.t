#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };

# Tests Urb::Boundary length bug

my $quad = Urb::Quad->new;
$quad->Deserialise (YAML::LoadFile ('t/data/test2.dom'));

#  binary tree
#       '' 
#      /  \
#     l    r
#         /  \
#        rl  rr
#       /  \    
#     rll  rlr   

#  +----+---+--------+
#  |rlr |rll| rr     |
#  +----+---+--------+
#  |                 |
#  |                 |
#  |    l            |
#  +-----------------+

my $boundaries = $quad->Calc_Boundaries;

my $boundary = $boundaries->{''};
ok ($boundary->Validate_Ids, 'boundary validates');

is (scalar @{$boundary}, 4, 'boundary');

is ($boundary->[0]->{quad}->Id, 'l', 'Id()');
is ($boundary->[1]->{quad}->Id, 'rll', 'Id()');
is ($boundary->[2]->{quad}->Id, 'rlr', 'Id()');
is ($boundary->[3]->{quad}->Id, 'rr', 'Id()');

is ($boundary->[0]->{quad}->Length ($boundary->[0]->{id_edge}), 16.0, 'Length()');
is ($boundary->[1]->{quad}->Length ($boundary->[1]->{id_edge}), 2.25, 'Length()');
is ($boundary->[2]->{quad}->Length ($boundary->[2]->{id_edge}), 3.75, 'Length()');
is ($boundary->[3]->{quad}->Length ($boundary->[3]->{id_edge}), 10.0, 'Length()');

is ($boundary->Overlap ($quad->By_Relative_Id ('l'), $quad->By_Relative_Id ('rr')), 10.0, 'Overlap()');
is ($boundary->Overlap ($quad->By_Relative_Id ('l'), $quad->By_Relative_Id ('rll')), 2.25, 'Overlap()');
is ($boundary->Overlap ($quad->By_Relative_Id ('l'), $quad->By_Relative_Id ('rlr')), 3.75, 'Overlap()');

is ($boundary->Length_Total, 16.0, 'Length_Total()');

1;
