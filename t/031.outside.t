#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;

my $dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/bad-outside.dom')), 'deserialise');

# in the test file, 'rl' contains a four storey building, but 'l' and 'rr' are
# full of junky 'outside' quads all the way up.

$dom->Merge_Divided;
$dom->Split_Undivided_Up;
$dom->Highest->Split_Undivided_Down;

ok (!$dom->By_Id ('rrrrrrr'), 'By_Id()');
ok (!$dom->Above->By_Id ('rrrrrrr'), 'By_Id()');
ok (!$dom->Above->Above->By_Id ('rrrrrrr'), 'By_Id()');
ok (!$dom->Above->Above->Above->By_Id ('rrrrrrr'), 'By_Id()');
ok (!$dom->By_Id ('lllll'), 'By_Id()');
ok (!$dom->Above->By_Id ('lllll'), 'By_Id()');
ok (!$dom->Above->Above->By_Id ('lllll'), 'By_Id()');
ok (!$dom->Above->Above->Above->By_Id ('lllll'), 'By_Id()');
ok (!$dom->By_Id ('lr'), 'By_Id()');
ok (!$dom->Above->By_Id ('lr'), 'By_Id()');
ok (!$dom->Above->Above->By_Id ('lr'), 'By_Id()');
ok (!$dom->Above->Above->Above->By_Id ('lr'), 'By_Id()');
ok (!$dom->By_Id ('rrl'), 'By_Id()');
ok (!$dom->Above->By_Id ('rrl'), 'By_Id()');
ok (!$dom->Above->Above->By_Id ('rrl'), 'By_Id()');
ok (!$dom->Above->Above->Above->By_Id ('rrl'), 'By_Id()');

1;
