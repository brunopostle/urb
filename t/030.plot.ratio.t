#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;

my $dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/multilevel.dom')), 'deserialise');

like ($dom->Area_Internal, '/^151\.1/', 'Area_Internal()');

like ($dom->Area, '/^166/', 'Area()');

like ($dom->Plot_Ratio, '/^0\.91/', 'Plot_Ratio()');

1;
