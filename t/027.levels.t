#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;

my $dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/multilevel.dom')), 'deserialise');

ok ($dom->Above->L->L->Is_Supported, 'Is_Supported()');
ok ($dom->Above->R->R->L->L->Is_Supported, 'Is_Supported()');
ok (!$dom->Above->R->L->Is_Supported, 'Is_Supported()');
ok (!$dom->R->L->Is_Supported, 'Is_Supported()');
ok (!$dom->R->L->Is_Covered, 'Is_Covered()');
ok ($dom->L->R->R->Is_Covered, 'Is_Covered()');

is ($dom->Levels_Above, 1, '2 levels, 1 level above');
ok ($dom->Divided, 'divided');

my @leafs0 = $dom->Leafs;
is (scalar @leafs0, 8, '8 leafs on level 0');
my @leafs1 = $dom->Above->Leafs;
is (scalar @leafs1, 9, '9 leafs on level 1');

ok ($dom->Above->Clone_Above, 'clone level 1 with Clone_Above()');
is ($dom->Levels_Above, 2, '3 levels, 2 level above');

@leafs0 = $dom->Leafs;
is (scalar @leafs0, 8, '8 leafs on level 0');
@leafs1 = $dom->Above->Leafs;
is (scalar @leafs1, 9, '9 leafs on level 1');
my @leafs2 = $dom->Above->Above->Leafs;
is (scalar @leafs2, 9, '9 leafs on level 2');

ok ($dom->Above->Del_Above, 'Del_Above()');
is ($dom->Levels_Above, 1, '2 levels, 1 level above');

ok ($dom->Clone_Above, 'clone level 0 with Clone_Above()');
is ($dom->Levels_Above, 2, '3 levels, 2 level above');

@leafs0 = $dom->Leafs;
is (scalar @leafs0, 8, '8 leafs on level 0');
@leafs1 = $dom->Above->Leafs;
is (scalar @leafs1, 8, '8 leafs on level 1');
@leafs2 = $dom->Above->Above->Leafs;
is (scalar @leafs2, 9, '9 leafs on level 2');

$dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/supported.dom')), 'deserialise');
ok (!$dom->Above->Is_Supported, 'partially supported outdoor is not \'supported\'');
ok (!$dom->Above->Above->Is_Supported, 'outdoor above outdoor isn\'t \'supported\'');

1;
