#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use Data::Dumper;
use Test::More 'no_plan';
use 5.010;
use Urb::Topologist;

my ($VAR1, $VAR2);

$VAR1 = [ [ -14.158327102661133, 3.0046632289886475, 6.999999523162842 ],
          [ -14.158327102661133, 3.0046639442443848, 9.999999046325684 ],
          [ -13.705097198486328, 5.499413013458252, 10.0 ],
          [ -13.705097198486328, 5.49941349029541, 6.999999523162842 ] ];
$VAR2 = [ [ -15.241947174072266, -2.860834836959839, 7.0 ],
          [ -15.241947174072266, -2.8608341217041016, 10.0 ],
          $VAR1->[1],
          $VAR1->[0]
        ];

is (Urb::Topologist::_el(1.5999997), 1.6);
is (Urb::Topologist::_el(1.6000013), 1.6);
is (Urb::Topologist::_el(-1.5999997), -1.6);
is (Urb::Topologist::_el(-1.6000013), -1.6);
is (Urb::Topologist::_el(0.0000013), 0.0);
is (Urb::Topologist::_el(-0.0000013), 0.0);

my $t = Urb::Topologist->new([{coor => [0,0,0]},
                              {coor => [1,0,0]},
                              {coor => [1,1,0]},
                              {coor => [0,1,0]},
                              {coor => [0,0,1]},
                              {coor => [1,0,1]},
                              {coor => [1,1,1]},
                              {coor => [0,1,1]},
                              {coor => [2,0,0]},
                              {coor => [2,1,0]},
                              {coor => [2,1,1]},
                              {coor => [2,0,1]}
                          ],
                          [{node_id => [0,1,2,3]},
                           {node_id => [4,5,6,7]},
                           {node_id => [0,1,5,4]},
                           {node_id => [2,3,7,6]},
                           {node_id => [1,2,6,5]},
                           {node_id => [0,3,7,4]},
                           {node_id => [1,8,9,2]},
                           {node_id => [1,8,11,5]},
                           {node_id => [9,8,11,10]},
                           {node_id => [2,9,10,6]},
                           {node_id => [5,11,10,6]}
                       ], {});

$t->build_graph_nodes;
$t->build_graph_faces;
$t->build_graph_cells;

for my $cell ($t->cells)
{
    next if $cell->is_world;
    for my $face ($cell->faces)
    {
        my @triangles = $face->to_triangles;
        is(scalar(@triangles),2, 'two triangles');
        is(scalar(@{$_}), 3, 'three points per triangle') for @triangles;
        is($face->area, 1, 'area()');
    }
    is($cell->plan_area, 1, 'plan_area()');
    is($cell->external_wall_area, 3, 'external_area()');
    is($cell->crinkliness, 3, 'crinkliness()');
    is($cell->centroid->[2], 0.5, 'centroid()');
}

1;
