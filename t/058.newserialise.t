#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use Data::Dumper;
use Test::More 'no_plan';
use Urb::Dom;
use YAML;
use 5.010;

my $dom_simple = Urb::Dom->new;
my @yaml_simple_old = YAML::LoadFile('t/data/simple.dom');
ok($dom_simple->Deserialise(@yaml_simple_old), 'deserialise old format');

my @yaml_simple_new = $dom_simple->Serialise;
is($yaml_simple_new[0]->{rotation}, 0, 'new format as expected');

my $dom = Urb::Dom->new;
my @yaml_old = YAML::LoadFile('t/data/sixteenth.dom');
ok($dom->Deserialise(@yaml_old), 'deserialise old format');

my @yaml_new = $dom->Serialise;
is($yaml_new[0]->{above}->{r}->{l}->{type}, 'O', 'new format as expected');

my $dom_new = Urb::Dom->new;
ok($dom_new->Deserialise(@yaml_new), 'deserialise new format');

is_deeply ($dom, $dom_new, 'deserialised serialised dom is identical');

1;
