#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;

my $dom_a = Urb::Dom->new;
ok ($dom_a->Deserialise (YAML::LoadFile ('t/data/multilevel.dom')), 'deserialise');

is (scalar @{[$dom_a->Leafs]},               8, '8 leafs on a/0');
is (scalar @{[$dom_a->Left->Leafs]},         5, '5 leafs on a/0/l');
is (scalar @{[$dom_a->Right->Leafs]},        3, '3 leafs on a/0/r');

is (scalar @{[$dom_a->Above->Leafs]},        9, '9 leafs on a/1');
is (scalar @{[$dom_a->Above->Left->Leafs]},  5, '5 leafs on a/1/l');
is (scalar @{[$dom_a->Above->Right->Leafs]}, 4, '4 leafs on a/1/r');

my $dom_b = Urb::Dom->new;
ok ($dom_b->Deserialise (YAML::LoadFile ('t/data/multilevel.dom')), 'deserialise');

is (scalar @{[$dom_b->Leafs]},               8, '8 leafs on a/0');
is (scalar @{[$dom_b->Left->Leafs]},         5, '5 leafs on b/0/l');
is (scalar @{[$dom_b->Right->Leafs]},        3, '3 leafs on a/0/r');

is (scalar @{[$dom_b->Above->Leafs]},        9, '9 leafs on a/1');
is (scalar @{[$dom_b->Above->Left->Leafs]},  5, '5 leafs on a/1/l');
is (scalar @{[$dom_b->Above->Right->Leafs]}, 4, '4 leafs on a/1/r');


ok ($dom_a->Above->Crossover ($dom_b->Left), 'Crossover()');

# these bits are the same as before
is (scalar @{[$dom_a->Leafs]},               8, '8 leafs on a/0');
is (scalar @{[$dom_a->Left->Leafs]},         5, '5 leafs on a/0/l');
is (scalar @{[$dom_a->Right->Leafs]},        3, '3 leafs on a/0/r');

is (scalar @{[$dom_b->Above->Leafs]},        9, '9 leafs on b/1');
is (scalar @{[$dom_b->Above->Left->Leafs]},  5, '5 leafs on a/1/l');
is (scalar @{[$dom_b->Above->Right->Leafs]}, 4, '4 leafs on a/1/r');

is (scalar @{[$dom_b->Right->Leafs]},        3, '3 leafs on a/0/r');

# these bits should be different
is (scalar @{[$dom_a->Above->Leafs]},        5, '5 leafs on a/1');

is (scalar @{[$dom_b->Leafs]},              12, '12 leafs on b/0');
is (scalar @{[$dom_b->Left->Leafs]},         9, '9 leafs on a/0/l');
is (scalar @{[$dom_b->Left->Left->Leafs]},   5, '5 leafs on a/1/l');
is (scalar @{[$dom_b->Left->Right->Leafs]},  4, '4 leafs on a/1/r');

$dom_b->Del_Above;
$dom_b->Undivide;

is (scalar @{[$dom_b->Levels_Above]}, 0, '1 level');
is (scalar @{[$dom_b->Leafs]}, 1, '1 leaf');

1;
