#!/usr/bin/perl

#Editor vim:syn=perl

use 5.010;
use strict;
use warnings;
use Test::More 'no_plan';
use lib ('lib', '../lib');
use_ok ('Urb::Dom');

my $dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/multilevel.dom')), 'deserialise');

my $graph = $dom->Graph;
my $clone = $graph->clone;

1;
