#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };

use Urb;

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

my $quad = Urb::Quad->new;

$quad->{node} = [[1.0,1.0],[4.0,2.0],[4.0,6.0],[2.0,4.0]];

is ($quad->Position, '', 'position of root is empty string');
is ($quad->Id, '', 'Root level Id is empty string');
is ($quad->Coordinate (1)->[0], 4.0, 'Coordinate');
is ($quad->Coordinate (1)->[1], 2.0, 'Coordinate');
is ($quad->Divided, 0, 'Not divided');
is ($quad->Coordinate_a, undef, 'division coordinates undefined');

is ($quad->Boundary_Id (0), 'a', 'boundary id');
is ($quad->Boundary_Id (1), 'b', 'boundary id');
is ($quad->Boundary_Id (2), 'c', 'boundary id');
is ($quad->Boundary_Id (3), 'd', 'boundary id');

$quad->Divide (0.5, 0.5);

is ($quad->Boundary_Id (0), 'a', 'boundary id');
is ($quad->Boundary_Id (1), 'b', 'boundary id');
is ($quad->Boundary_Id (2), 'c', 'boundary id');
is ($quad->Boundary_Id (3), 'd', 'boundary id');

is ($quad->Left->Boundary_Id (0), 'a', 'boundary id');
is ($quad->Left->Boundary_Id (1), '', 'boundary id');
is ($quad->Left->Boundary_Id (2), 'c', 'boundary id');
is ($quad->Left->Boundary_Id (3), 'd', 'boundary id');

is ($quad->Coordinate_a->[0], 2.5, 'division coordinates defined');
is ($quad->Coordinate_a->[1], 1.5, 'division coordinates defined');
is ($quad->Coordinate_b->[0], 3.0, 'division coordinates defined');
is ($quad->Coordinate_b->[1], 5.0, 'division coordinates defined');

$quad->Rotate;

is ($quad->Left->Boundary_Id (0), 'b', 'boundary id');  
is ($quad->Left->Boundary_Id (1), '', 'boundary id');
is ($quad->Left->Boundary_Id (2), 'd', 'boundary id');
is ($quad->Left->Boundary_Id (3), 'a', 'boundary id');

is ($quad->Boundary_Id (0), 'b', 'boundary id');
is ($quad->Boundary_Id (1), 'c', 'boundary id');
is ($quad->Boundary_Id (2), 'd', 'boundary id');
is ($quad->Boundary_Id (3), 'a', 'boundary id');

is ($quad->Coordinate (0)->[0], 4.0, 'Coordinate');
is ($quad->Coordinate (0)->[1], 2.0, 'Coordinate');

is ($quad->Coordinate_a->[0], 4.0, 'division coordinates defined');
is ($quad->Coordinate_a->[1], 4.0, 'division coordinates defined');
is ($quad->Coordinate_b->[0], 1.5, 'division coordinates defined');
is ($quad->Coordinate_b->[1], 2.5, 'division coordinates defined');

my $child = $quad->Right;

is ($child->Position, 'r', 'child position is r');
is ($child->Id, 'r', 'child Id is r');

ok ($quad->Swap, 'swap returns success');

is ($child->Position, 'l', 'child position is l');
is ($child->Id, 'l', 'child Id is l');

ok ($quad->Swap, 'swap returns success');

is ($child->Coordinate (0)->[0], 4.0, 'Coordinate');
is ($child->Coordinate (0)->[1], 4.0, 'Coordinate');

is ($child->Coordinate (1)->[0], 4.0, 'Coordinate');
is ($child->Coordinate (1)->[1], 6.0, 'Coordinate');

is ($child->Coordinate (2)->[0], 2.0, 'Coordinate');
is ($child->Coordinate (2)->[1], 4.0, 'Coordinate');

is ($child->Coordinate (3)->[0], 1.5, 'Coordinate');
is ($child->Coordinate (3)->[1], 2.5, 'Coordinate');

$quad->Left->Divide (0.5, 0.5);
$quad->Right->Divide (0.8, 0.8);
$quad->Right->Rotate;

$quad->Right->Right->Divide (0.3, 0.3);
$quad->Left->Right->Divide (0.7, 0.7);
$quad->Left->Right->Rotate;

$quad->Right->Right->Rotate;

# () Rotation=1 Coords=4,2 4,6 2,4 1,1
#     (l) Rotation=0 Coords=4,2 4,4 1.5,2.5 1,1
#         (ll) Rotation=0 Coords=4,2 4,3 1.25,1.75 1,1 (LEAF)
#         (lr) Rotation=1 Coords=4,4 1.5,2.5 1.25,1.75 4,3
#             (lrl) Rotation=0 Coords=4,4 2.25,2.95 2.075,2.125 4,3 (LEAF)
#             (lrr) Rotation=0 Coords=2.25,2.95 1.5,2.5 1.25,1.75 2.075,2.125 (LEAF)
#     (r) Rotation=1 Coords=4,6 2,4 1.5,2.5 4,4
#         (rl) Rotation=0 Coords=4,6 2.4,4.4 2,2.8 4,4 (LEAF)
#         (rr) Rotation=1 Coords=2,4 1.5,2.5 2,2.8 2.4,4.4
#             (rrl) Rotation=0 Coords=2,4 1.85,3.55 2.28,3.92 2.4,4.4 (LEAF)
#             (rrr) Rotation=0 Coords=1.85,3.55 1.5,2.5 2,2.8 2.28,3.92 (LEAF)

is ($quad->By_Id ('')->Centroid->[0], 2.75, 'centroid');
is ($quad->By_Id ('')->Centroid->[1], 3.25, 'centroid');
like ($quad->By_Id ('lrr')->Centroid->[0], '/1.768/', 'centroid');
like ($quad->By_Id ('lrr')->Centroid->[1], '/2.331/', 'centroid');

$quad->Shift (2,3,4);
like ($quad->By_Id ('lrr')->Centroid->[0], '/3.768/', 'centroid');
like ($quad->By_Id ('lrr')->Centroid->[1], '/5.331/', 'centroid');
is ($quad->Elevation, 4, 'Elevation()');

#  binary tree
#         '' 
#       /     \
#     l        r
#    /  \     /  \
#  ll  lr   rl  rr
#     /   \   /   \
#    lrl lrr rrl rrr

#     quad Ids           boundary Ids
#  +---------+------+  +----c----+--c---+
#  |   rrl   |      |  d         r      |
#  +---------+  rl  |  +---rr----+      b
#  |   rrr   |      |  d         r      |
#  +------+--+------+  +--''--+''+--''--+
#  |  lrr |    lrl  |  d      lr        b
#  +------+---------+  +---l--+-----l---+
#  |        ll      |  d                b
#  +----------------+  +-------a--------+

is ($quad->Hash, $quad->Hash, 'Hash() repeatable');
isnt ($quad->Hash, $quad->L->R->R->Hash, 'Hash() is different where expected');

is ($quad->Hash_Site, $quad->Hash_Site, 'Hash_Site() repeatable');
is ($quad->Hash_Site, $quad->L->R->R->Hash_Site, 'Hash_Site() repeatable however');
