#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;
use Urb::Dom::Fitness;
use YAML;

my $dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/ml.bad.dom')), 'deserialise');

my $level = $dom->Above;
my $graph = $level->Graph;

my $boundaries = $level->Calc_Boundaries;

for my $id (keys %{$boundaries})
{
   my $boundary = $boundaries->{$id};
   is ($id, $boundary->Id, "$id label ok");
   ok ($boundary->Validate_Ids, 'Validate_Ids()');
   if ($id eq 'l')
   {
       ok ($boundary->Overlap ($level->By_Id ('llr'), $level->By_Id ('lrll')) > 0.1, 'llr:lrll');
       ok ($boundary->Overlap ($level->By_Id ('llr'), $level->By_Id ('lrlr')) < 0.1, 'llr:lrlr');
       #die map {'  '. $_->[0]->Id .':'. $_->[1]->Id} @{$boundary->Pairs};
   } 
}

my $lr = $level->By_Id ('lr');
my $lrl = $level->By_Id ('lrl');
my $lrlr = $level->By_Id ('lrlr');

is ($lr->{rotation}, 1, 'lr->{rotation} == 1');
is ($lr->Rotation, 0, 'lr->Rotation == 0');

is ($lrl->{rotation}, 1, 'lrl->{rotation} == 1');
is ($lrl->Rotation, 0, 'level1 lrl->Rotation == 0');
is ($dom->L->R->L->Rotation, 0, 'level0 lrl->Rotation == 0');

ok ($lrlr->Boundary_Id (0) eq 'a', '0 eq lr');
ok ($lrlr->Boundary_Id (1) eq 'lr', '1 eq \'\'');
ok ($lrlr->Boundary_Id (2) eq '', '2 eq lrl');
ok ($lrlr->Boundary_Id (3) eq 'lrl', '3 eq a');

ok ($graph->has_edge ($dom->Above->L->R->L->R, $dom->Above->L->R->R), 'real edge between 1/lrlr and 1/lrr');

ok (!$graph->has_edge ($dom->Above->R->L->L->L, $dom->Above->L->R->R), 'bogus edge between 1/lrlr and 1/lrr');

1;
