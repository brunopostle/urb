#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;
use Urb::Dom::Fitness;
use YAML;

my $dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/types.dom')), 'deserialise');

my $ratios = $dom->Ratios;

ok (Urb::Dom::Fitness::ratio_o ($ratios, 0.5, 0.2) > 0.96, 'o 0.5 ok');
ok (Urb::Dom::Fitness::ratio_o ($ratios, 0.5549, 0.2) > 0.99, 'o 0.5549 ok');
ok (Urb::Dom::Fitness::ratio_o ($ratios, 0.8, 0.2) < 0.5, 'o 0.8 nearly ok');
ok (Urb::Dom::Fitness::ratio_o ($ratios, 0.2, 0.2) < 0.3, 'o 0.2 ok');

ok (Urb::Dom::Fitness::ratio_type ($ratios, 't', 0.0849, 0.2) > 0.99, 't');
ok (Urb::Dom::Fitness::ratio_type ($ratios, 'c', 0.2137, 0.2) > 0.99, 'c');
ok (Urb::Dom::Fitness::ratio_type ($ratios, 'k', 0.1966, 0.2) > 0.99, 'k');
ok (Urb::Dom::Fitness::ratio_type ($ratios, 'l', 0.2359, 0.2) > 0.99, 'l');
ok (Urb::Dom::Fitness::ratio_type ($ratios, 'b', 0.2689, 0.2) > 0.99, 'b');

ok (Urb::Dom::Fitness::ratio_type ($ratios, 'c', 0.0, 0.2) =~ /^0\.5/x, 'c');
ok (Urb::Dom::Fitness::ratio_type ($ratios, 't', 0.06, 0.025) =~ /^0\.6/x, 't');
ok (Urb::Dom::Fitness::ratio_type ($ratios, 'k', 0.1, 0.1) =~ /^0\.6/x, 'k');
ok (Urb::Dom::Fitness::ratio_type ($ratios, 'l', 0.15, 0.1) =~ /^0\.6/x, 'l');
ok (Urb::Dom::Fitness::ratio_type ($ratios, 'b', 0.4, 0.15) =~ /^0\.6/x, 'b');

# +-----+---------+-----+-----+
# |lrll |lrlrr    |lrr  |rr   |
# |O    |O        |L    |O    |
# |     +---------+     |     |
# |     |lrlrl    |     +-----+
# |     |C        |     |rlr  |
# +-----+-+-----+-+-----+K    |
# |lllrlr |lllrr|llr    |     |
# |T      |C    |O      |     |
# +-------+     |       +-----+
# |lllrll |     |       |rll  |
# |B      |     |       |O    |
# +-----+-+-----+       |     |
# |lllll|llllr  |       |     |
# |O    |B      |       |     |
# +-----+-------+-------+-----+

my $count = $dom->Count;

is (scalar @{$count->{o}}, 6, '6 O');
is (scalar @{$count->{c}}, 2, '2 C');
is (scalar @{$count->{l}}, 1, '1 L');
is (scalar @{$count->{k}}, 1, '1 K');
is (scalar @{$count->{b}}, 2, '2 B');
is (scalar @{$count->{t}}, 1, '1 T');
