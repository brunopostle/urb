#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;
use Urb::Field::Occlusion;

my $dom = Urb::Dom->new;

$dom->{node} = [[0.0,0.0],[16.0,0.0],[16.0,16.0],[0.0,16.0]];
$dom->{perimeter} = {a => 'private', b => undef, c => undef, d => undef};

$dom->Type ('O');

my @walls = $dom->Walls;
is (scalar (@walls), 0, 'no walls');

$dom->Divide (0.5, 0.5);
$dom->R->Type ('O');
$dom->L->Type ('L');

@walls = $dom->Walls;
is (scalar (@walls), 4, 'four walls');

$dom->L->Divide (0.5, 0.5);
$dom->L->L->Type ('C');
$dom->L->R->Type ('L');

#use YAML; YAML::DumpFile ('walls.dom', $dom->Serialise);

@walls = sort {$a->[2] cmp $b->[2]} $dom->Walls;
is (scalar (@walls), 6, 'six walls');

is ($walls[2]->[0]->[2], 0, 'elevation');
is ($walls[2]->[1]->[2], 3, 'top');
is ($walls[1]->[2], 'party', 'party');

is ($walls[3]->[0]->[2], 0, 'elevation');
is ($walls[3]->[1]->[2], 3, 'top');
is ($walls[2]->[2], 'private', 'private');

is ($walls[5]->[0]->[2], 0, 'elevation');
is ($walls[5]->[1]->[2], 3, 'top');
is ($walls[5]->[2], 'public', 'public');

#use Data::Dumper; die Dumper @walls;

my $field = Urb::Field::Occlusion->new;
$field->{_walls} = \@walls;

my $intersections = $field->Walls_intersecting ([-1,1,1], 0.001);
is (scalar @{$intersections}, 2, '2 intersections');
$intersections = $field->Walls_intersecting ([-1,1,1], 0.0);
is (scalar @{$intersections}, 2, '2 intersections');

$intersections = $field->Walls_intersecting ([1,1,1], 0.001);
is (scalar @{$intersections}, 1, '1 intersections');
$intersections = $field->Walls_intersecting ([1,1,1], 0.0);
is (scalar @{$intersections}, 1, '1 intersections');

$intersections = $field->Walls_intersecting ([1,-1,1], 0.001);
is (scalar @{$intersections}, 0, '0 intersections');

$intersections = $field->Walls_intersecting ([1,-1,1], 1.57);
is (scalar @{$intersections}, 2, '2 intersections');

$intersections = $field->Walls_intersecting ([1,-1,1], -1.57);
is (scalar @{$intersections}, 0, '0 intersections');

$intersections = $field->Walls_intersecting ([1,20,1], -1.57);
is (scalar @{$intersections}, 2, '2 intersections');

my $p1 = $field->Walls_intersecting ([1,1,1], 0.0);
my $p2 = $field->Walls_intersecting ([1,1,1], 0.1);

ok ($p1->[0]->[0] > $p2->[0]->[0], 'inclination');
is ($p1->[0]->[1], $p2->[0]->[1], 'type');

my ($low, $high) = $field->Walls_lowhigh;
is ($low, 0, 'low 0.0');
is ($high, 3, 'high 3.0');

#use Data::Dumper; die Dumper $p1, $p2;

ok ($field->CIEsky_horizontal ([9,8,0]) < $field->CIEsky_horizontal ([10,8,0]), 'CIEsky_horizontal lighter further from wall');
ok ($field->CIEsky_horizontal ([10,8,0]) < $field->CIEsky_horizontal ([11,8,0]), 'CIEsky_horizontal lighter even further from wall');
ok ($field->CIEsky_horizontal ([12,8,0]) < $field->CIEsky_horizontal ([12,9,0]), 'CIEsky_horizontal lighter nearer corner');
ok ($field->CIEsky_horizontal ([12,8,0]) < $field->CIEsky_horizontal ([12,3,0]), 'CIEsky_horizontal lighter nearer corner');

ok ($field->CIEsky_vertical ([9,8,0], 0.0) > $field->CIEsky_vertical ([9,8,0], 3.142), 'CIEsky_vertical');
ok ($field->CIEsky_vertical ([9,8,0], 0.0) == $field->CIEsky_vertical ([10,8,0], 0.0), 'CIEsky_vertical');
ok ($field->CIEsky_vertical ([9,8,0], 3.142) < $field->CIEsky_vertical ([10,8,0], 3.142), 'CIEsky_vertical');
ok ($field->CIEsky_vertical ([9,8,0], 3.142) < $field->CIEsky_vertical ([9,8,0], 1.571), 'CIEsky_vertical');

ok ($field->CIEsky_vertical ([9,10,0], 3.142) < $field->CIEsky_vertical ([9,10,0], 1.571), 'CIEsky_vertical');
ok ($field->CIEsky_vertical ([9,10,0], -1.571) < $field->CIEsky_vertical ([9,10,0], 1.571), 'CIEsky_vertical');

like ($field->CIEsky_horizontal ([9.5,8.1,0]), '/^2.057/','cache interpolation');
like ($field->CIEsky_vertical ([9.5,8.1,0], 3.142), '/^0.138/','cache interpolation');

$field->{nocache} = 1;

like ($field->CIEsky_horizontal ([9.5,8.1,0]), '/^2.071/', 'nocache no interpolation');
like ($field->CIEsky_vertical ([9.5,8.1,0], 3.142), '/^0.145/', 'nocache no interpolation');

like ($dom->R->Bearing (0), '/^4.71/', 'Urb::Quad::Bearing()');
like ($dom->R->Bearing (1), '/^0/', 'Urb::Quad::Bearing()');
like ($dom->R->Bearing (2), '/^1.57/', 'Urb::Quad::Bearing()');
like ($dom->R->Bearing (3), '/^3.14/', 'Urb::Quad::Bearing()');

my $graph = $dom->Graph;
my $boundaries = $dom->Calc_Boundaries;

my $vertex = $dom->L->R;

for my $neighbour ($graph->neighbours ($vertex))
{
    my $coordinates = $dom->Graph->get_edge_attribute ($vertex, $neighbour, 'coordinates');
    is (scalar @{$coordinates}, 2, 'two coordinates');
    is (scalar @{$coordinates->[0]}, 2, 'coordinates have two values');

    for my $key_boundary (keys %{$boundaries})
    {
        my $overlap = $boundaries->{$key_boundary}->Overlap ($vertex, $neighbour);
        next if $overlap == 0;
#        warn $boundaries->{$key_boundary}->Bearing ($vertex, $neighbour);
    }
}

like ($boundaries->{''}->Bearing ($vertex, $dom->R), '/^0/', 'Urb::Boundary::Bearing()');
like ($boundaries->{l}->Bearing ($vertex, $dom->L->L), '/^3.14/', 'Urb::Boundary::Bearing()');

1;
