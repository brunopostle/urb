#!/usr/bin/perl

#Editor vim:syn=perl

use 5.010;
use strict;
use warnings;
use Test::More 'no_plan';
use lib ('lib', '../lib');
BEGIN { use_ok('Urb::Polygon') };

# to_quads

ok (my $poly = Urb::Polygon->new ([1,2], [2,4], [1,5], [1,2]), 'new');

ok (my @lines = $poly->_lines, '_lines');
is (scalar @lines, 3, '3 lines');

ok (my @gr = $poly->_gradients, '_gradients');
is (scalar @gr, 3, '3 gradiets');

is ($gr[0] +0, 2, '_gradient');
is ($gr[1] +0, -1, '_gradient');
ok (abs($gr[2]) < 100000000000, '_gradient');

my $poly_small = Urb::Polygon->new ([1.5,3], [2,4], [1,5], [1.5,3]);
my $poly_overlap = Urb::Polygon->new ([1.5,-3], [2,4], [1,5], [1.5,-3]);

ok ($poly_small->_is_contained_by ($poly), '_is_contained_by');
ok ($poly_overlap->_overlaps ($poly), '_overlaps');

ok ($poly->_is_on_polygon ([1,2]), '_is_on_polygon');
ok ($poly->_is_on_polygon ([2,4]), '_is_on_polygon');
ok ($poly->_is_on_polygon ([1,5]), '_is_on_polygon');

ok ($poly->_is_on_polygon ([1.5,3]), '_is_on_polygon');
ok ($poly->_is_on_polygon ([1.9,4.1]), '_is_on_polygon');
ok ($poly->_is_on_polygon ([1,3]), '_is_on_polygon');

ok (!$poly->_is_on_polygon ([2,3]), '_is_on_polygon');
ok (!$poly->_is_on_polygon ([1.5,4]), '_is_on_polygon');

ok (my $poly2 = $poly->_divide_polygon, '_divide_polygon');
ok (@lines = $poly2->_lines, '_lines');
is (scalar @lines, 6, '6 lines');

my $z = Urb::Polygon->new ([1.1,0], [3.1,0], [3.2,2.1], [5.1,2.1], [5.2,7], [2.1,7], [2.3,2], [1.3,2], [1.1,0]);

ok (my @quads = $z->to_quads, 'to_quads');
is (scalar @quads, 3, '3 quads');
is (scalar $quads[0]->_lines, 4, 'a quad');
is (scalar $quads[1]->_lines, 4, 'a quad');
is (scalar $quads[2]->_lines, 4, 'a quad');

my $triangles = $z->to_triangles;
is (scalar @{$triangles}, 6, 'six triangles');
is_deeply ($triangles->[0], [0,1,2,0], 'vertex ids');
is_deeply ($triangles->[5], [0,2,6,0], 'vertex ids');

is ($z->nrPoints, 9, 'nine points inc. start at end');
my $triangle = $z->_ear_clip;
is ($triangle->nrPoints, 4, 'triangle has four points');

is ($z->nrPoints, 8, 'eight points');
$triangle = $z->_ear_clip;
is ($z->nrPoints, 7, 'seven points');
$triangle = $z->_ear_clip;
is ($z->nrPoints, 6, 'six points');
$triangle = $z->_ear_clip;
is ($z->nrPoints, 5, 'five points');
$triangle = $z->_ear_clip;
is ($z->nrPoints, 4, 'four points');
$triangle = $z->_ear_clip;
is ($z->nrPoints, 0, 'zero points');

1;
