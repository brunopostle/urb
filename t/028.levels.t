#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;

my $dom = Urb::Dom->new;
ok ($dom->Deserialise (YAML::LoadFile ('t/data/multilevel.dom')), 'deserialise');

is ($dom->Levels_Above, 1, '2 levels, 1 level above');
ok ($dom->Divided, 'divided');

my @leafs0 = $dom->Leafs;
is (scalar @leafs0, 8, '8 leafs on level 0');
my @leafs1 = $dom->Above->Leafs;
is (scalar @leafs1, 9, '9 leafs on level 1');

is ($dom->Above->Below, $dom, 'check Above->Below is circular reference');

ok (!$dom->Above->Swap_Above, 'Swap_Above() fails if nothing above');
ok ($dom->Swap_Above, 'Swap_Above() success');

is ($dom->Above->Below, $dom, 'check Above->Below is circular reference');

is ($dom->Levels_Above, 1, '2 levels, 1 level above');

@leafs0 = $dom->Leafs;
is (scalar @leafs0, 9, '9 leafs on level 0');
@leafs1 = $dom->Above->Leafs;
is (scalar @leafs1, 8, '8 leafs on level 1');

ok (!$dom->Above->Swap_Above, 'Swap_Above() fails if nothing above');
ok ($dom->Swap_Above, 'Swap_Above() success');

is ($dom->Above->Below, $dom, 'check Above->Below is circular reference');

@leafs0 = $dom->Leafs;
is (scalar @leafs0, 8, '8 leafs on level 0');
@leafs1 = $dom->Above->Leafs;
is (scalar @leafs1, 9, '9 leafs on level 1');

ok ($dom->Above->Clone_Above, 'clone level 1 with Clone_Above()');
is ($dom->Levels_Above, 2, '3 levels, 2 level above');

is ($dom->Above->Below, $dom, 'check Above->Below is circular reference');
is ($dom->Above->Above->Below, $dom->Above, 'check Above->Below is circular reference');

my @leafs2 = $dom->Above->Above->Leafs;
is (scalar @leafs2, 9, '9 leafs on level 2');

ok ($dom->Swap_Above, 'Swap_Above() success');

is ($dom->Above->Below, $dom, 'check Above->Below is circular reference');
is ($dom->Above->Above->Below, $dom->Above, 'check Above->Below is circular reference');

@leafs0 = $dom->Leafs;
is (scalar @leafs0, 9, '9 leafs on level 0');
@leafs1 = $dom->Above->Leafs;
is (scalar @leafs1, 8, '8 leafs on level 1');
@leafs2 = $dom->Above->Above->Leafs;
is (scalar @leafs2, 9, '9 leafs on level 2');

ok ($dom->Above->Swap_Above, 'Swap_Above() success');

is ($dom->Above->Below, $dom, 'check Above->Below is circular reference');
is ($dom->Above->Above->Below, $dom->Above, 'check Above->Below is circular reference');

@leafs0 = $dom->Leafs;
is (scalar @leafs0, 9, '9 leafs on level 0');
@leafs1 = $dom->Above->Leafs;
is (scalar @leafs1, 9, '9 leafs on level 1');
@leafs2 = $dom->Above->Above->Leafs;
is (scalar @leafs2, 8, '8 leafs on level 2');

is ($dom->Perimeter->{d}, 'fortified', 'Perimeter()');
is ($dom->Perimeter->{c}, 'private', 'Perimeter()');
is ($dom->Above->Perimeter->{c}, 'private', 'Perimeter()');
is ($dom->Above->Above->Perimeter->{c}, 'private', 'Perimeter()');

1;
