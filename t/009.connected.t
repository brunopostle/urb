#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use Data::Dumper;

use Test::More 'no_plan';
use strict;
use warnings;
use Urb::Dom;

my $dom = Urb::Dom->new;

$dom->{node} = [[0.0,0.0],[16.0,0.0],[16.0,16.0],[0.0,16.0]];

$dom->Divide (0.75, 0.75);
$dom->L->Divide (0.6, 0.6);
$dom->L->L->Divide (0.5, 0.5);
$dom->L->L->L->Divide (0.4, 0.4);
$dom->L->L->L->L->Divide (0.3, 0.3);
$dom->L->L->L->L->Unrotate;
$dom->L->L->L->R->Divide (0.7, 0.7);
$dom->L->L->L->R->L->Divide (0.6, 0.6);
$dom->L->L->L->R->L->Rotate;
$dom->L->L->L->R->Unrotate;
$dom->L->L->L->Rotate;
$dom->L->L->Unrotate;
$dom->L->R->Divide (0.65, 0.65);
$dom->L->R->L->Divide (0.3, 0.3);
$dom->L->R->L->R->Divide (0.4, 0.4);
$dom->L->R->L->R->Rotate;
$dom->L->R->Unrotate;
$dom->L->Rotate;
$dom->R->Divide (0.7, 0.7);
$dom->R->L->Divide (0.5, 0.5);
$dom->R->Rotate;

# +-----+---------+-----+-----+
# |lrll |lrlrr    |lrr  |rr   |
# |O    |O        |L    |O    |
# |     +---------+     |     |
# |     |lrlrl    |     +-----+
# |     |C        |     |rlr  |
# +-----+-+-----+-+-----+K    |
# |lllrlr |lllrr|llr    |     |
# |T      |C    |O      |     |
# +-------+     |       +-----+
# |lllrll |     |       |rll  |
# |B      |     |       |O    |
# +-----+-+-----+       |     |
# |lllll|llllr  |       |     |
# |O    |B      |       |     |
# +-----+-------+-------+-----+

$dom->L->L->L->L->L->Type ('O');
$dom->L->L->L->L->R->Type ('B');
$dom->L->L->L->R->L->L->Type ('B');
$dom->L->L->L->R->L->R->Type ('T');
$dom->L->L->L->R->R->Type ('C');
$dom->L->L->R->Type ('O');
$dom->L->R->L->L->Type ('O');
$dom->L->R->L->R->L->Type ('C');
$dom->L->R->L->R->R->Type ('O');
$dom->L->R->R->Type ('L');
$dom->R->L->L->Type ('O');
$dom->R->L->R->Type ('K');
$dom->R->R->Type ('O');

my $graph = $dom->Graph;
ok ($dom->Connected_Circulation ($graph), 'two nodes connected');
$dom->R->L->L->Type ('C');
$graph = $dom->Graph;
ok (! $dom->Connected_Circulation ($graph), 'three nodes not connected');
$dom->R->L->L->Type ('O');
$graph = $dom->Graph (1.25);

is ((join ':', $dom->R->R->Neighbour_Types ($graph)), 'K:L','rr is connected to K:L types');
is ((join ':', $dom->L->R->R->Neighbour_Types ($graph)), 'C:K:O:O:O','lrr is connected to C:L:O:O:O types');

for my $leaf ($dom->Leafs)
{
    if ($leaf->Type =~ /^k/xi)
    {
        ok ((grep {/^(l|c)/xi} $leaf->Neighbour_Types ($graph)), 'K has C or L connection');
    }
    elsif ($leaf->Type =~ /^o/xi)
    {
        ok (1, 'O doesn\'t need connections');
        next;
    }
    else
    {
        ok ((grep {/^c/xi} $leaf->Neighbour_Types ($graph)), 'has C connection');
    }
}

for my $leaf ($dom->Leafs)
{
    ok (scalar $leaf->Access ($graph) > 0, 'Has access');
}

for my $leaf ($dom->Leafs)
{
    next if $leaf->Type =~ /^o/xi;
    ok (scalar $leaf->Access_Outside ($graph) > 0, 'Has access outside');
}

ok (! scalar $dom->L->L->L->R->R->Access_External, 'lllrr has no external boundary');
is ((join ':', $dom->R->L->L->Access_External), 'a:b', 'rll has two external boundaries');
is ((join ':', $dom->R->R->Access_External), 'b:c', 'rr has two external boundaries');

for my $leaf ($dom->Leafs)
{
    next if $leaf->Type =~ /^o/xi;
    next if $leaf->Id eq 'lllrr';
}

my $ratios = $dom->Ratios;
is (scalar keys %{$ratios}, 6, '6 types');
my $sum = 0.0;
for my $key (keys %{$ratios}) { $sum += $ratios->{$key} }
is ($sum, 1, 'ratios add up to 1');

ok (!$dom->L->L->L->L->L->Public_Access_Outside ($graph), 'lllll doesn\'t have public access outside');
ok ($dom->L->L->R->Public_Access_Outside ($graph), 'llr has public access outside');
ok ($dom->R->L->L->Public_Access_Outside ($graph), 'rll has public access outside');
$dom->{perimeter} = {a => undef, b => undef, c => undef, d => undef};
ok (!$dom->L->L->L->L->L->Public_Access_Outside ($graph), 'lllll doesn\'t have public access outside');
ok ($dom->L->L->R->Public_Access_Outside ($graph), 'llr has public access outside');
ok ($dom->R->L->L->Public_Access_Outside ($graph), 'rll has public access outside');
ok ($dom->R->R->Public_Access_Outside ($graph), 'rr has public access outside');
ok (!$dom->R->L->R->Public_Access_Outside ($graph), 'rlr doesn\'t have public access outside');

exit;
