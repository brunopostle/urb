#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Quad') };

# Tests for methods that query Urb::Quad Ids

my $quad = Urb::Quad->new;
$quad->Deserialise (YAML::LoadFile ('t/data/quad.dom'));

#  binary tree
#         '' 
#       /     \
#     l        r
#    /  \     /  \
#  ll  lr   rl  rr
#     /   \   /   \
#    lrl lrr rrl rrr

#     quad Ids           boundary Ids
#  +---------+------+  +----c----+--c---+
#  |   rrl   |      |  d         r      |
#  +---------+  rl  |  +---rr----+      b
#  |   rrr   |      |  d         r      |
#  +------+--+------+  +--''--+''+--''--+
#  |  lrr |    lrl  |  d      lr        b
#  +------+---------+  +---l--+-----l---+
#  |        ll      |  d                b
#  +----------------+  +-------a--------+

is ($quad->Left->Left->Root->Id, '', 'root Id is empty string');
is ($quad->Left->Right->Root->Id, '', 'root Id is empty string');
is ($quad->Right->Root->Id, '', 'root Id is empty string');

is ($quad->By_Id ('rrr')->Id, 'rrr', 'By_Id');
is ($quad->Left->By_Id ('rrr')->Id, 'rrr', 'By_Id');
is ($quad->Left->Right->By_Id ('rrr')->Id, 'rrr', 'By_Id');

is ($quad->By_Id ('')->Id, '', 'By_Id');

1;
