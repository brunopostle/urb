#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use YAML;
use Data::Dumper;

use Test::More 'no_plan';
BEGIN { use_ok('Urb::Dom') };

my $quad = Urb::Dom->new;
$quad->Deserialise (YAML::LoadFile ('t/data/types.dom'));

# +-----+---------+-----+-----+
# |lrll |lrlrr    |lrr  |rr   |
# |O    |O        |L    |O    |
# |     +---------+     |     |
# |     |lrlrl    |     +-----+
# |     |C        |     |rlr  |
# +-----+-+-----+-+-----+K    |
# |lllrlr |lllrr|llr    |     |
# |T      |C    |O      |     |
# +-------+     |       +-----+
# |lllrll |     |       |rll  |
# |B      |     |       |O    |
# +-----+-+-----+       |     |
# |lllll|llllr  |       |     |
# |O    |B      |       |     |
# +-----+-------+-------+-----+

my $graph = $quad->Graph;

my $occlusion = $quad->Occlusion;

is ($quad->By_Id ('lllrr')->Length_Outside ($graph), 5.76, 'Length_Outside()');
is ($quad->By_Id ('lrlrl')->Length_Outside ($graph), 9.82, 'Length_Outside()');
$quad->L->L->R->{type} = 'K';
ok (!$quad->By_Id ('lllrr')->Length_Outside ($graph), 'Length_Outside()');
$quad->L->L->R->{type} = 'O';

# street edge plus garden edge
is ($quad->R->L->L->Length_Outside ($graph), 9.6, 'Length_Outside()');

like ($quad->By_Id ('lllrr')->Crinkliness ($graph, $occlusion), '/^(1.66666666|1.47)/', 'Crinkliness()');

$quad->{perimeter} = {a => undef, b => undef, c => 'private', d => 'private'};

is ($quad->By_Id ('lllrr')->Public_Length, 0.0, 'Public_length()');
is ($quad->By_Id ('lrlrl')->Public_Length, 0.0, 'Public_length()');
is ($quad->By_Id ('lrll')->Public_Length, 0.0, 'Public_length()');
is ($quad->By_Id ('lrr')->Public_Length, 0.0, 'Public_length()');

is ($quad->By_Id ('lllll')->Public_Length, 1.8, 'Public_length()');
is ($quad->By_Id ('llllr')->Public_Length, 4.2, 'Public_length()');
is ($quad->By_Id ('llr')->Public_Length, 6.0, 'Public_length()');
is ($quad->By_Id ('rll')->Public_Length, 9.6, 'Public_length()');
is ($quad->By_Id ('rlr')->Public_Length, 5.6, 'Public_length()');
is ($quad->By_Id ('rr')->Public_Length, 4.8, 'Public_length()');

is ($quad->By_Id ('lllrr')->Private_Length, 0.0, 'Private_length()');
is ($quad->By_Id ('lrlrl')->Private_Length, 0.0, 'Private_length()');
is ($quad->By_Id ('lrll')->Private_Length, 8.74, 'Private_length()');
is ($quad->By_Id ('lrr')->Private_Length, 4.2, 'Private_length()');

is ($quad->By_Id ('lllll')->Private_Length, 3.84, 'Private_length()');
is ($quad->By_Id ('llllr')->Private_Length, 0.0, 'Private_length()');
is ($quad->By_Id ('llr')->Private_Length, 0.0, 'Private_length()');
is ($quad->By_Id ('rll')->Private_Length, 0.0, 'Private_length()');
is ($quad->By_Id ('rlr')->Private_Length, 0.0, 'Private_length()');
is ($quad->By_Id ('rr')->Private_Length, 4.0, 'Private_length()');

1;
