#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use Data::Dumper;
use Test::More 'no_plan';
use YAML;
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use File::DXF::ENTITIES;
use Urb::Topologist;
use 5.010;

my ($dxf, @report, $first_mesh, $nodes, $faces, $metadata, $topo, $elev, $height, $graph, $elev0, $elev1);

##### non-manifold-1

$dxf = File::DXF->new;
$dxf->Process(read_DXF('t/data/non-manifold-1.dxf', 'CP1252'));
@report = $dxf->{ENTITIES}->Report('POLYLINE', 'VERTEX', 'SEQEND');
($first_mesh) = File::DXF::ENTITIES::polyface_to_meshes(@report);
($nodes, $faces, $metadata) = @{$first_mesh};

$topo = Urb::Topologist->new($nodes, $faces);

$topo->build_graph_nodes;
$topo->build_graph_faces;
$topo->build_graph_cells;
$topo->allocate_cells();

is (scalar $topo->{graph_nodes}->vertices, 32, 'graph_nodes');
is (scalar $topo->{graph_faces}->vertices, 37, 'graph_faces');
is (scalar $topo->{graph_cells}->vertices, 7, 'graph_cells');
is (scalar $topo->{graph_cells}->edges, 12, 'graph_cells');

is (scalar $topo->cells, 7, 'cells');

is (scalar $topo->faces_vertical, 23, 'faces_vertical');
is (scalar $topo->faces_vertical_internal, 5, 'faces_vertical_internal');
is (scalar $topo->faces_vertical_external, 18, 'faces_vertical_external');
is (scalar $topo->faces_horizontal, 14, 'faces_horizontal');
is (scalar $topo->faces_horizontal_internal, 2, 'faces_horizontal_internal');
is (scalar $topo->faces_horizontal_external, 12, 'faces_horizontal_external');


is (ref $topo->walls_external, 'HASH', 'walls_external');
is (scalar keys %{$topo->walls_external}, 2, 'walls_external');
($elev0, $elev1) = sort keys %{$topo->walls_external};

is ($elev0, 0.0, 'walls_external');
is (ref $topo->walls_external->{$elev0}, 'HASH', 'walls_external');
is (scalar keys %{$topo->walls_external->{$elev0}}, 1, 'walls_external');
($height) = keys %{$topo->walls_external->{$elev0}};
like ($height, '/^3\.06/', 'walls_external');
$graph = $topo->walls_external->{$elev0}->{$height};
is (scalar $graph->vertices, 12, 'walls_external');
is (scalar $graph->edges, 12, 'walls_external');

like ($elev1, '/^3\.06/', 'walls_external');
is (ref $topo->walls_external->{$elev1}, 'HASH', 'walls_external');
is (scalar keys %{$topo->walls_external->{$elev1}}, 1, 'walls_external');
($height) = keys %{$topo->walls_external->{$elev1}};
like ($height, '/^2\.8/', 'walls_external');
$graph = $topo->walls_external->{$elev1}->{$height};
is (scalar $graph->vertices, 6, 'walls_external');
is (scalar $graph->edges, 6, 'walls_external');


is (ref $topo->walls_eaves, 'HASH', 'walls_eaves');
is (scalar keys %{$topo->walls_eaves}, 2, 'walls_eaves');
($elev0, $elev1) = sort keys %{$topo->walls_eaves};

is ($elev0, 0.0, 'walls_eaves');
is (ref $topo->walls_eaves->{$elev0}, 'HASH', 'walls_eaves');
is (scalar keys %{$topo->walls_eaves->{$elev0}}, 1, 'walls_eaves');
($height) = keys %{$topo->walls_eaves->{$elev0}};
like ($height, '/^3\.06/', 'walls_eaves');
$graph = $topo->walls_eaves->{$elev0}->{$height};
is (scalar $graph->vertices, 11, 'walls_eaves');
is (scalar $graph->edges, 9, 'walls_eaves');

like ($elev1, '/^3\.06/', 'walls_eaves');
is (ref $topo->walls_eaves->{$elev1}, 'HASH', 'walls_eaves');
is (scalar keys %{$topo->walls_eaves->{$elev1}}, 1, 'walls_eaves');
($height) = keys %{$topo->walls_eaves->{$elev1}};
like ($height, '/^2\.8/', 'walls_eaves');
$graph = $topo->walls_eaves->{$elev1}->{$height};
is (scalar $graph->vertices, 6, 'walls_eaves');
is (scalar $graph->edges, 6, 'walls_eaves');


is (ref $topo->walls_internal, 'HASH', 'walls_internal');
is (scalar keys %{$topo->walls_internal}, 2, 'walls_internal');
($elev0, $elev1) = sort keys %{$topo->walls_internal};

is ($elev0, 0.0, 'walls_internal');
is (ref $topo->walls_internal->{$elev0}, 'HASH', 'walls_internal');
is (scalar keys %{$topo->walls_internal->{$elev0}}, 1, 'walls_internal');
($height) = keys %{$topo->walls_internal->{$elev0}};
like ($height, '/^3\.06/', 'walls_internal');
$graph = $topo->walls_internal->{$elev0}->{$height};
is (scalar $graph->vertices, 5, 'walls_internal');
is (scalar $graph->edges, 4, 'walls_internal');

like ($elev1, '/^3\.06/', 'walls_internal');
is (ref $topo->walls_internal->{$elev1}, 'HASH', 'walls_internal');
is (scalar keys %{$topo->walls_internal->{$elev1}}, 1, 'walls_internal');
($height) = keys %{$topo->walls_internal->{$elev1}};
like ($height, '/^2\.8/', 'walls_internal');
$graph = $topo->walls_internal->{$elev1}->{$height};
is (scalar $graph->vertices, 2, 'walls_internal');
is (scalar $graph->edges, 1, 'walls_internal');

is (ref $topo->cells_internal, 'HASH', 'cells_internal');
is (scalar keys %{$topo->cells_internal}, 2, 'cells_internal');
($elev0, $elev1) = sort keys %{$topo->cells_internal};

is ($elev0, 0.0, 'cells_internal');
is (ref $topo->cells_internal->{$elev0}, 'ARRAY', 'cells_internal');
is (scalar @{$topo->cells_internal->{$elev0}}, 4, 'cells_internal');
like ($elev1, '/^3\.06/', 'cells_internal');
is (ref $topo->cells_internal->{$elev1}, 'ARRAY', 'cells_internal');
is (scalar @{$topo->cells_internal->{$elev1}}, 2, 'cells_internal');

##### non-manifold-2

$dxf = File::DXF->new;
$dxf->Process(read_DXF('t/data/non-manifold-2.dxf', 'CP1252'));
@report = $dxf->{ENTITIES}->Report('POLYLINE', 'VERTEX', 'SEQEND');
($first_mesh) = File::DXF::ENTITIES::polyface_to_meshes(@report);
($nodes, $faces, $metadata) = @{$first_mesh};

$topo = Urb::Topologist->new($nodes, $faces);

$topo->build_graph_nodes;
$topo->build_graph_faces;
$topo->build_graph_cells;

is (scalar $topo->{graph_nodes}->vertices, 32, 'graph_nodes');
is (scalar $topo->{graph_faces}->vertices, 37, 'graph_faces');
is (scalar $topo->{graph_cells}->vertices, 7, 'graph_cells');
is (scalar $topo->{graph_cells}->edges, 12, 'graph_cells');

is (scalar $topo->cells, 7, 'cells non-manifold-2');

is (scalar $topo->faces_vertical, 23, 'faces_vertical');
is (scalar $topo->faces_vertical_internal, 5, 'faces_vertical_internal');
is (scalar $topo->faces_vertical_external, 18, 'faces_vertical_external');
is (scalar $topo->faces_horizontal, 14, 'faces_horizontal');
is (scalar $topo->faces_horizontal_internal, 2, 'faces_horizontal_internal');
is (scalar $topo->faces_horizontal_external, 12, 'faces_horizontal_external');


is (ref $topo->walls_external, 'HASH', 'walls_external');
is (scalar keys %{$topo->walls_external}, 2, 'walls_external');
($elev0, $elev1) = sort keys %{$topo->walls_external};

is ($elev0, 0.0, 'walls_external');
is (ref $topo->walls_external->{$elev0}, 'HASH', 'walls_external');
is (scalar keys %{$topo->walls_external->{$elev0}}, 1, 'walls_external');
($height) = keys %{$topo->walls_external->{$elev0}};
like ($height, '/^3\.06/', 'walls_external');
$graph = $topo->walls_external->{$elev0}->{$height};
is (scalar $graph->vertices, 12, 'walls_external');
is (scalar $graph->edges, 12, 'walls_external');

like ($elev1, '/^3\.06/', 'walls_external');
is (ref $topo->walls_external->{$elev1}, 'HASH', 'walls_external');
is (scalar keys %{$topo->walls_external->{$elev1}}, 1, 'walls_external');
($height) = keys %{$topo->walls_external->{$elev1}};
like ($height, '/^2\.8/', 'walls_external');
$graph = $topo->walls_external->{$elev1}->{$height};
is (scalar $graph->vertices, 6, 'walls_external');
is (scalar $graph->edges, 6, 'walls_external');


is (ref $topo->walls_eaves, 'HASH', 'walls_eaves');
is (scalar keys %{$topo->walls_eaves}, 2, 'walls_eaves');
($elev0, $elev1) = sort keys %{$topo->walls_eaves};

is ($elev0, 0.0, 'walls_eaves');
is (ref $topo->walls_eaves->{$elev0}, 'HASH', 'walls_eaves');
is (scalar keys %{$topo->walls_eaves->{$elev0}}, 1, 'walls_eaves');
($height) = keys %{$topo->walls_eaves->{$elev0}};
like ($height, '/^3\.06/', 'walls_eaves');
$graph = $topo->walls_eaves->{$elev0}->{$height};
is (scalar $graph->vertices, 11, 'walls_eaves');
is (scalar $graph->edges, 9, 'walls_eaves');

like ($elev1, '/^3\.06/', 'walls_eaves');
is (ref $topo->walls_eaves->{$elev1}, 'HASH', 'walls_eaves');
is (scalar keys %{$topo->walls_eaves->{$elev1}}, 1, 'walls_eaves');
($height) = keys %{$topo->walls_eaves->{$elev1}};
like ($height, '/^2\.8/', 'walls_eaves');
$graph = $topo->walls_eaves->{$elev1}->{$height};
is (scalar $graph->vertices, 6, 'walls_eaves');
is (scalar $graph->edges, 6, 'walls_eaves');


is (ref $topo->walls_internal, 'HASH', 'walls_internal');
is (scalar keys %{$topo->walls_internal}, 2, 'walls_internal');
($elev0, $elev1) = sort keys %{$topo->walls_internal};

is ($elev0, 0.0, 'walls_internal');
is (ref $topo->walls_internal->{$elev0}, 'HASH', 'walls_internal');
is (scalar keys %{$topo->walls_internal->{$elev0}}, 1, 'walls_internal');
($height) = keys %{$topo->walls_internal->{$elev0}};
like ($height, '/^3\.06/', 'walls_internal');
$graph = $topo->walls_internal->{$elev0}->{$height};
is (scalar $graph->vertices, 5, 'walls_internal');
is (scalar $graph->edges, 4, 'walls_internal');

like ($elev1, '/^3\.06/', 'walls_internal');
is (ref $topo->walls_internal->{$elev1}, 'HASH', 'walls_internal');
is (scalar keys %{$topo->walls_internal->{$elev1}}, 1, 'walls_internal');
($height) = keys %{$topo->walls_internal->{$elev1}};
like ($height, '/^2\.8/', 'walls_internal');
$graph = $topo->walls_internal->{$elev1}->{$height};
is (scalar $graph->vertices, 2, 'walls_internal');
is (scalar $graph->edges, 1, 'walls_internal');

is (ref $topo->cells_internal, 'HASH', 'cells_internal');
is (scalar keys %{$topo->cells_internal}, 2, 'cells_internal');
($elev0, $elev1) = sort keys %{$topo->cells_internal};

is ($elev0, 0.0, 'cells_internal');
is (ref $topo->cells_internal->{$elev0}, 'ARRAY', 'cells_internal');
is (scalar @{$topo->cells_internal->{$elev0}}, 4, 'cells_internal');
like ($elev1, '/^3\.06/', 'cells_internal');
is (ref $topo->cells_internal->{$elev1}, 'ARRAY', 'cells_internal');
is (scalar @{$topo->cells_internal->{$elev1}}, 2, 'cells_internal');

my $count = 0;
for ($topo->cells) { $count++ if $_->point_is_inside([3,3,1]) }
is ($count, 2, 'point_is_inside');

$count = 0;
for ($topo->cells) { $count++ if $_->point_is_inside([6,4,1]) }
is ($count, 2, 'point_is_inside');

$count = 0;
for ($topo->cells) { $count++ if $_->point_is_inside([6,4,-1]) }
is ($count, 0, 'point_is_inside');

$count = 0;
for ($topo->cells) { $count++ if $_->point_is_inside([6,4,11]) }
is ($count, 0, 'point_is_inside');

$count = 0;
for ($topo->cells) { $count++ if $_->point_is_inside([3,7,1]) }
is ($count, 0, 'point_is_inside');

1;
