Topologise
==========

[2020 November] warning this documentation for this experimental tool may become out of date.

Topologise is a proof of concept, it decomposes non-manifold 3D meshes into
cells (rooms/spaces), storeys, and wall elements suitable for the Molior
library to compose into IFC solid geometry.

This is EXPERIMENTAL software, expect it to be slow, incomplete or broken.

QUICKSTART
----------

Process an example DXF input file, generate an intermediate MOLIOR file and
process this into an IFC file:

    urb-topologise.pl t/data/non-manifold-9.dxf non-manifold-9.molior
    molior-ifc.pl non-manifold-9.molior non-manifold-9.ifc

If this is ok, then there is a blender add-on (written in Python) which you can
install from within Blender, this is a single file here:

    blender/topologise.py

This add-on requires the BlenderBIM add-on and the pyYAML module.

INSTALLATION and DEPENDENCIES
-----------------------------

The Urb module requires some perl modules, see README.

The Topologise tool requires these additional perl modules:

    File::IFC
    File::DXF
    Molior

COPYRIGHT AND LICENCE
---------------------

Copyright (C) 2004-2020 by Bruno Postle <bruno@postle.net>

This file is part of Urb.

Urb is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Urb is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Urb.  If not, see <http://www.gnu.org/licenses/>.
