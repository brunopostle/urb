#!/usr/bin/perl

use strict;
use warnings;
use lib qw/lib/;
use Urb::Dom;
use YAML;
use Carp;

my $path_yaml = shift @ARGV;

croak "Usage: $0 /path/to/dom.dom X-shift Y-shift [Z-shift]" unless scalar @ARGV;

my $dom = Urb::Dom->new;
$dom->Deserialise (YAML::LoadFile ($path_yaml));

$dom->Shift (@ARGV);

YAML::DumpFile ($path_yaml, $dom->Serialise);

0;
