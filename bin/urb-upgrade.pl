#!/usr/bin/perl

use strict;
use warnings;
use lib qw/lib/;
use Urb::Dom;
use YAML;
use 5.010;

for my $path_yaml (@ARGV)
{
    next unless $path_yaml =~ /\.dom$/x;
    next unless -e $path_yaml;
    say STDERR $path_yaml;
    my $dom = Urb::Dom->new;
    $dom->Deserialise(YAML::LoadFile($path_yaml)) || next;
    next unless scalar @{$dom->{node}} == 4;
    YAML::DumpFile($path_yaml, $dom->Serialise);
}

exit 0;
