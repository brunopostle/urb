#!/usr/bin/perl

use strict;
use warnings;
use lib qw/lib/;
use Urb::Dom;
use YAML;

for my $path_yaml (@ARGV)
{
    my $dom = Urb::Dom->new;
    $dom->Deserialise (YAML::LoadFile ($path_yaml));

    my $perimeter = $dom->Perimeter;
    my $node = $dom->{node};

    for my $key (keys %{$perimeter})
    {
        $perimeter->{$key} = 'public' unless defined $perimeter->{$key};
    }

    my $text;
    $text .= join ',', @{$node->[0]};
    $text .= ' ['. $perimeter->{a} .'] ';
    $text .= join ',', @{$node->[1]};
    $text .= ' ['. $perimeter->{b} .'] ';
    $text .= join ',', @{$node->[2]};
    $text .= ' ['. $perimeter->{c} .'] ';
    $text .= join ',', @{$node->[3]};
    $text .= ' ['. $perimeter->{d} .'] ';
    print STDOUT $text ."\n";
}
