#!/usr/bin/perl

use strict;
use warnings;
use lib qw/lib/;
use Urb::Dom;
use YAML;
use Carp;
use 5.010;

my $path_yaml = shift @ARGV;

croak "Usage: $0 /path/to/dom.dom" unless -e $path_yaml;

my $dom = Urb::Dom->new;
$dom->Deserialise (YAML::LoadFile ($path_yaml));

# clean out any existing structure
$dom->Del_Above;
$dom->Undivide;
$dom->Type ('C');
$dom->{height} = 3.0;

$dom->Clone_Above;
$dom->Clone_Above;

my @queue = ($dom, $dom->Above, $dom->Above->Above);

while (scalar (@queue))
{
    push @queue, _doit (shift @queue);
}

YAML::DumpFile ($path_yaml, $dom->Serialise);

0;

sub _doit
{
    my $self = shift;
    return () if $self->Length_Narrowest < 5.0;
    $self->Divide (0.333, 0.333);
    $self->Rotation (int rand (4));

    if ($self->Left->Length_Narrowest < 2.5 or $self->Left->Length_Narrowest < 2.5)
    {
        $self->Divide (0.5, 0.5);
    }

    for my $leaf ($self->Left, $self->Right)
    {
        my @types = qw/T C K L B O/;
        $leaf->Type ($types[int rand (scalar @types)]);
    }

    return ($self->Left, $self->Right);
}
