#!/usr/bin/perl

use strict;
use warnings;
use 5.010;
use Carp;
use YAML;
use lib 'lib';
use Urb::Field::Occlusion;
use Urb::Math;

our $PI = atan2 (0,-1);

croak "Usage: $0 occlusion.field 12.5 3.0 1.7" unless @ARGV == 4;

my $path_yaml = shift;
open my $YAML, '<', $path_yaml or croak "$!";
my $string = join '', (<$YAML>);
close $YAML;

my $field = Urb::Field::Occlusion->new;
$field->Deserialise ($string);

my $size_x = scalar @{$field->{_data}};
my $size_y = scalar @{$field->{_data}->[0]};
my $size_z = scalar @{$field->{_data}->[0]->[0]};
my $directions = scalar @{$field->{_data}->[0]->[0]->[0]};

my $coor = [@ARGV];

my $scale = 8;

my @mask = map {$field->Get_interpolated_position ($coor, $_)} (0 .. 15);
say join '  ', 'Altitude:', @mask;

use Urb::Misc::Sun;
my $sun = Urb::Misc::Sun->new (53.3814);

my @minutes = map {$sun->Minutes ((4 - $_) * 0.392699081, $mask[$_])} (0 .. 15);
say join '  ', 'minutes:', @minutes;

0;
