#!/usr/bin/perl

use strict;
use warnings;
use lib qw/lib/;
use Urb::Dom::Output;
use YAML;
use Carp;
use File::Spec::Functions qw /rel2abs splitpath splitdir/;

for my $path_yaml (@ARGV)
{
    my $path_obj = $path_yaml.'.obj';
    my $obj = "# Homemaker OBJ file\n# https://bitbucket.org/brunopostle/urb\n";
    my $i = 1;

    my ($vol, $dirs, $filename) = splitpath (rel2abs ($path_yaml));
    my @split_dirs = splitdir ($dirs);
    pop @split_dirs if $split_dirs[-1] eq '';
    my $plot_name = pop @split_dirs;
    $obj .= "o $plot_name\n";

    my $dom = Urb::Dom::Output->new;
    $dom->Deserialise (YAML::LoadFile ($path_yaml));

    ($obj, $i) = $dom->OBJ_mesh($obj, $i);
    ($obj, $i) = $dom->OBJ_widgets($obj, $i);

    open my $OBJFILE, ">:encoding(UTF-8)", $path_obj or croak "$!";
    print $OBJFILE $obj;
    close $OBJFILE;
}
