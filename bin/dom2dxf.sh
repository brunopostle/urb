#!/bin/sh

# Builds DXF and DAE equivalent 3D models of an Urb::Dom file.
# Requires File::DXF and Molior.

# generate a MOLIOR file
urb-dom2molior.pl "$1" _all.molior

# generate a DXF file
molior.pl _all.molior "$1.dxf"

# generate a collada DAE file
polyline2collada.pl "$1.dxf" "$1.dae"

# generate an IFC file
molior-ifc.pl _all.molior "$1.ifc"

# delete intermediate files
rm _all.molior

