#!/usr/bin/perl

use strict;
use warnings;
use lib qw/lib/;
use Urb::Dom;
use YAML;
use Carp;

my $path_yaml = shift @ARGV;

croak "Usage: $0 /path/to/dom.dom OFFSET" unless scalar @ARGV;
# negative is an 'inset', positive is an 'outset'

my $offset = shift;

my $dom = Urb::Dom->new;
$dom->Deserialise (YAML::LoadFile ($path_yaml));

my @new_node = map {$dom->Coordinate_Offset ($_, $offset)} (0 .. 3);
$dom->{node} = [@new_node];

YAML::DumpFile ($path_yaml, $dom->Serialise);

0;
