#!/usr/bin/perl

use strict;
use warnings;
use Cwd;
use File::Copy;
use File::Spec::Functions qw/catdir/;
use 5.010;

local $SIG{INT} = \&interrupt;
local $SIG{HUP} = \&interrupt;
local $SIG{TERM} = \&interrupt;

evolve(max_iterations(), max_pop(), max_procs());

sub evolve
{
    my $max_iterations = shift;
    my $max_pop = shift;
    my $max_procs = shift;

    my $count = 1;
    for (1 .. $max_iterations)
    {
        say "Iteration $count of $max_iterations";
        multiply($max_pop, $max_procs);
        fitness($max_procs);
        system(reaper());
        $count++;
    }
    my ($path_best, $score_best) = best(Cwd::cwd());
    copy($path_best, 'best.dom');
    system ('urb-fitness.pl', 'best.dom');
    system 'sync';
}

sub interrupt
{
    say STDERR "[$$] caught signal";
    system ('urb-fitness.pl');
    system ('urb-reap.pl');
    my ($path_best, $score_best) = best(Cwd::cwd());
    copy($path_best, 'best.dom');
    system ('urb-fitness.pl', 'best.dom');
    system 'sync';
    exit 0;
}

sub multiply
{
    my $max_pop = shift;
    my $max_procs = shift;
    my @file = glob ('*.dom');

    $max_procs = $max_pop if $max_procs > $max_pop;
    my $no_batch = int($max_pop/$max_procs);

    for (1 .. $no_batch)
    {
        for (1 .. $max_procs)
        {
            my $pid = fork();
            if ($pid == 0)
            {
                my $path_a = $file[int(rand(scalar(@file)))];
                my $path_b = $file[int(rand(scalar(@file)))];
                say "Combining $path_a $path_b";
                exec('urb-combine.pl', $path_a, $path_b);
            }
            elsif (!defined $pid)
            {
                warn "Fork failed:";
            }
        }
        1 while wait() >= 0;
    }
}

sub fitness
{
    my $max_procs = shift;
    my @file = glob ('*.dom');
    my $no_total = scalar(@file);
    $max_procs = $no_total if $max_procs > $no_total;
    my $no_batch = int($no_total/$max_procs);
    my @items;
    for (1 .. $max_procs)
    {
        push @items, [splice(@file, 0 , $no_batch)];
    }
    push @items, [@file] if scalar(@file);
    
    foreach (@items)
    {
        my $pid = fork();
        if ($pid == 0)
        {
            exec('urb-fitness.pl', @{$_});
        }
        elsif (!defined $pid)
        {
            warn "Fork failed:";
        }
    }

    1 while wait() >= 0;
    say(join(' ', best(Cwd::cwd())));
}

sub best
{
    my $path_plot = shift;

    my $lookup = {};
    opendir(DIR, $path_plot);
    while (my $name = readdir(DIR))
    {
        next unless $name =~ /\.score$/x;
        open my $SCORE, '<', catdir($path_plot, $name) or next;
        my @score = (<$SCORE>);
        my $score = shift @score;
        close($SCORE);
        $lookup->{$score} = $name;
    }
    closedir(DIR);
    my @scores = sort {$b <=> $a} keys %{$lookup};
    my $best = $lookup->{$scores[0]};
    chomp($scores[0]);
    $best =~ s/\.score$//x;
    return catdir($path_plot, $best), $scores[0];
}

sub max_iterations
{
    return 768 unless defined $ENV{MAX_ITERATIONS};
    return $ENV{MAX_ITERATIONS};
}

sub max_pop
{
    return 128 unless defined $ENV{MAX_POP};
    return $ENV{MAX_POP};
}

sub max_procs
{
    return 4 unless defined $ENV{MAX_PROCS};
    return $ENV{MAX_PROCS};
}

sub reaper
{
    return "urb-reap.pl" unless defined $ENV{REAPER};
    return $ENV{REAPER};
}

0;
