#!/usr/bin/perl

use strict;
use warnings;
use lib qw/lib/;
use Urb::Dom;
use YAML;

# calculates crinkliness of a plot, i.e. divides street frontage by area.

for my $path_yaml (@ARGV)
{
    my $dom = Urb::Dom->new;
    $dom->Deserialise (YAML::LoadFile ($path_yaml));
    print STDOUT $dom->Public_Length / $dom->Area ."\n";
}

0;
