#!/usr/bin/perl

use strict;
use warnings;
use lib qw/lib/;
use Urb::Dom::Output;
use YAML;
use Carp;

for my $path_yaml (@ARGV)
{
    my $path_dot = $path_yaml.'.tree.dot';

    my $dom = Urb::Dom::Output->new;
    $dom->Deserialise (YAML::LoadFile ($path_yaml));

    my ($stub) = $path_yaml =~ /(.*)\.dom$/x;
    my $text_dot = $dom->Dot_Tree ($stub);

    open my $DOT, ">:encoding(UTF-8)", $path_dot or croak "$!";
    print $DOT $text_dot;
    close $DOT;
}

0;
