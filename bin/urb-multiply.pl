#!/usr/bin/perl

use strict;
use warnings;

my $MAX_POP = $ENV{MAX_POP} || 64;

my @file = glob ('*.dom');

exit 1 unless scalar (@file);

for (1 .. $MAX_POP)
{
    my $index_a = int (rand (scalar (@file)));
    my $index_b = int (rand (scalar (@file)));
    system ('urb-combine.pl', $file[$index_a], $file[$index_b]);
    print 'Combining: '. $file[$index_a] .' '. $file[$index_b] ."\n";
}

exit 0;

__END__

=head1 NAME

urb-multiply - Randomly combine individuals from a population

=head1 SYNOPSIS

urb-combine.pl

=head1 DESCRIPTION

B<urb-combine> takes all L<Urb::Dom> objects in the current folder, selects
random pairs and combines them, the resulting L<Urb::Dom> objects are written
to the same folder.

The number of pairs chosen defaults to 64, this can be overidden by setting the
MAX_POP environment variable.

L<urb-combine.pl> is used to combine/crossover the pairs, this generates two
new individuals each time.

=head1 COPYRIGHT

Copyright (c) 2004-2013 Bruno Postle <bruno@postle.net>.

This file is part of Urb.

Urb is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Urb is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Urb.  If not, see <http://www.gnu.org/licenses/>.

=head1 SEE ALSO

L<Urb>

=cut

