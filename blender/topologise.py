bl_info = {
    "name": "Homemaker Topologise",
    "blender": (2, 80, 0),
    "category": "Object",
}

import bpy
import bmesh
import tempfile
import yaml
import subprocess
import logging
from blenderbim import import_ifc
from os import path, getenv
from sys import platform

class ObjectTopologise(bpy.types.Operator):
    """Object Homemaker Topologise"""
    bl_idname = "object.topologise"
    bl_label = "Topologise"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):

        meshes = []

        for object in context.selected_objects:

            bpy.ops.object.transform_apply(location = True, rotation = True, scale = True)
            depsgraph = bpy.context.evaluated_depsgraph_get()
            object = object.evaluated_get(depsgraph)
            # Get a BMesh representation
            bm = bmesh.new()   # create an empty BMesh
            bm.from_mesh(object.data)   # fill it in from a Mesh
            bmesh.ops.remove_doubles(bm, verts=bm.verts, dist=0.001)
            bm.verts.ensure_lookup_table()

            for f in bm.faces:
                if (len(f.verts) == 4 or len(f.verts) == 3): continue
                first_z = f.verts[0].co[2]
                all_same_z = 1
                for v in f.verts:
                    if (abs(v.co[2] - first_z) > 0.001): all_same_z = 0
                if (all_same_z == 1): bmesh.ops.triangulate(bm, faces=[f])

            bm.verts.ensure_lookup_table()

            vertices = []
            faces = []

            for v in bm.verts:
                vertices.append({'coor':list(v.co[:])})

            for f in bm.faces:
                face_vertices = []
                for v in f.verts:
                    face_vertices.append(v.index)
                faces.append({'node_id':face_vertices})

            meshes.append([vertices,faces])
            bpy.ops.object.hide_view_set(unselected=False)

        yaml_tmp = tempfile.NamedTemporaryFile(mode='w+b', suffix='.yaml', delete=False)
        molior_tmp = tempfile.NamedTemporaryFile(mode='w+b', suffix='.molior', delete=False)
        ifc_tmp = tempfile.NamedTemporaryFile(mode='w+b', suffix='.ifc', delete=False)

        # START workaround for running the perl tools within cygwin on Windows
        # you may have to install the yaml module in the Blender python interpreter:
        #   cd "C:\Program Files\Blender Foundation\Blender 2.83\2.83\python"
        #   bin\python.exe -m pip install pyyaml
        if (platform.startswith('win32')):
            PathString=getenv('path') + r';C:\cygwin64\bin'
            PathSplited = PathString.split(';')
            test_cygbin = ['cygwin', 'bin']
            res1 = [i for i in PathSplited if test_cygbin[0] in i]
            Cygbash = "Can't find Cygwin bash.exe"
            for res2 in res1:
                res3 = [i for i in test_cygbin if(i in res2)]
                if (bool(res3) == True):
                    if (res2[-3:] == test_cygbin[1]): res2 = res2+'\\'
                    res2 = res2 + 'bash.exe'
                    if (path.exists(res2) == True):
                        Cygbash = res2
                        break

            res1 = yaml_tmp.name.split('\\')
            i = 0
            for j in res1:
                if (i == 0):
                    yaml_cygtmp = '/cygdrive/'+j[0].lower()
                    i = 1
                else:
                    yaml_cygtmp = yaml_cygtmp + '/' + j

            res1 = molior_tmp.name.split('\\')
            i = 0
            for j in res1:
                if (i == 0):
                    molior_cygtmp = '/cygdrive/'+j[0].lower()
                    i = 1
                else:
                    molior_cygtmp = molior_cygtmp + '/' + j

            res1 = ifc_tmp.name.split('\\')
            i = 0
            for j in res1:
                if (i == 0):
                    ifc_cygtmp = '/cygdrive/'+j[0].lower()
                    i = 1
                else:
                    ifc_cygtmp = ifc_cygtmp + '/' + j
        # END workaround for running the perl tools within cygwin on Windows

        with open(yaml_tmp.name, 'w') as outfile:
            yaml.dump(meshes, outfile)

        if (platform.startswith('win32')):
            if (Cygbash == "Can't find Cygwin bash.exe"):
                subprocess.call(['perl.exe', r'C:\strawberry\urb\bin\urb-topologise.pl', yaml_tmp.name, molior_tmp.name])
                subprocess.call(['perl.exe', r'C:\strawberry\molior\bin\molior-ifc.pl', molior_tmp.name, ifc_tmp.name])
            else:
                subprocess.call([Cygbash, '-c', '. ${HOME}/.bash_profile; urb-topologise.pl '+yaml_cygtmp+' '+molior_cygtmp])
                subprocess.call([Cygbash, '-c', '. ${HOME}/.bash_profile; molior-ifc.pl '+molior_cygtmp+' '+ifc_cygtmp])
        else:
            subprocess.call(['urb-topologise.pl', yaml_tmp.name, molior_tmp.name])
            subprocess.call(['molior-ifc.pl', molior_tmp.name, ifc_tmp.name])

        logger = logging.getLogger('ImportIFC')

        ifc_import_settings = import_ifc.IfcImportSettings.factory(bpy.context, ifc_tmp.name, logger)
        ifc_importer = import_ifc.IfcImporter(ifc_import_settings)
        ifc_importer.execute()
        return {'FINISHED'}

def menu_func(self, context):
    self.layout.operator(ObjectTopologise.bl_idname)

def register():
    bpy.utils.register_class(ObjectTopologise)
    bpy.types.VIEW3D_MT_object.append(menu_func)

def unregister():
    bpy.utils.unregister_class(ObjectTopologise)
    bpy.types.VIEW3D_MT_object.remove(menu_func)

if __name__ == "__main__":
    register()
