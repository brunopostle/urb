all : all.ifc all.dae

# write default configuration to patterns.config and costs.config for manual tweaking
defaults : urb-default.pl

# generate .walls file from DXF LINE data with 'thickness'
%.walls : %.dxf
	dxf2walls.pl $< $@

# Some functions that may be useful for debugging
# WARNING, all the stuff below is handled better by the Homemaker module

# generate DXF POLYFACE data from .walls file
%.walls.dxf : %.walls
	walls23dface.pl $< _3dface.tmp.dxf
	3dface2polyline.pl _3dface.tmp.dxf $@
	rm _3dface.tmp.dxf

# generate a best.molior file in each of the sub-folders
molior :
	find ./ -mindepth 1 -type d | while read LINE; do cd $$LINE; \
	  cp -a `${MAKE} -s best` _best.tmp && mv _best.tmp best.dom; \
	  urb-dom2molior.pl best.dom best.molior; \
	cd -; done

# generate a single IFC file from .molior files in sub-folders
all.ifc : molior
	molior-ifc.pl */best.molior $@

# generate DXF versions of any .walls files in the current folder
walls :
	ls | grep \\.walls | while read WALLS; do ${MAKE} $$WALLS.dxf; done

# generate a single DXF file from .molior files in sub-folders & any .walls.dxf files
all.dxf : molior walls
	molior.pl */best.molior _all.tmp.dxf
	dxfcat.pl _all.tmp.dxf *.walls.dxf $@
	rm _all.tmp.dxf

# convert DXF to Collada
%.dae : %.dxf
	polyline2collada.pl $< $@

.PHONY : all defaults molior walls
