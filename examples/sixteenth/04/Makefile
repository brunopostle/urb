# find the best DOM project, generate IFC and Collada 3D geometry, and some SVG representations
all : best.dom best.molior best.ifc best.dae best.svg best.dom.tree.svg best.dom.net.svg

# write default configuration to patterns.config and costs.config for manual tweaking
defaults :
	urb-default.pl

# 2D SVG diagram
%.svg : %.dom
	urb-dom2svg.pl $<

# visualisation of binary tree structure
%.tree.dot : %
	urb-dom2tree.pl $<

%.tree.svg : %.tree.dot
	dot -Tsvg -Gpage="4,4" -Gsize="4,4" -Gratio=fill -Gcenter=1 < $< > $@

# visualisation of network graph
%.net.dot : %
	urb-dom2net.pl $<

%.net.svg : %.net.dot
	neato -Tsvg -Gpage="4,4" -Gsize="4,4" -Gratio=fill -Gcenter=1 < $< > $@

# Some functions that may be useful for debugging
# WARNING, all the stuff below is handled better by the Homemaker module

# collect walls from sibling plots and generate an occlusion field
occlusion.field : init.dom
	urb-dom2field.pl init.dom ../*/best.dom ../*.walls

# evolve new DOM file from empty plot
evolve : init.dom occlusion.field
	urb-evolve.pl $<

# generate .score and .fails files for all DOM files
score-all :
	urb-fitness.pl *.dom

# return the name of the highest scoring DOM file
best :
	@grep --files-with-matches `cat *.score | sort -n | tail -n1` *.score | sed 's/\.score$$//' | tail -n1

# update the best.dom file
best.dom : score-all best
	cp -a `${MAKE} -s best` _best.tmp && mv _best.tmp best.dom
	urb-fitness.pl best.dom

# Molior BIM data
%.molior : %.dom
	urb-dom2molior.pl $< $@

# Industry Foundation Classes BIM data
%.ifc : %.molior
	molior-ifc.pl $< $@

# Drawing Exchange Format 3D geometry
%.dxf : %.molior
	molior.pl $< $@

# Collada 3D geometry
%.dae : %.dxf
	polyline2collada.pl $< $@

.PHONY : all defaults evolve score-all best
